/***
 *  $Id$
 **
 *  File: model_fit.hpp
 *  Created: Aug 04, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef MODEL_FIT_HPP
#define MODEL_FIT_HPP

#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <tbb/spin_mutex.h>

#include "graph_util.hpp"


class estimator {
public:
    void init(int ri, int qi) { state_ = 0.0; }

    void finalize(int qi) { }

    void operator()(int Nij) { }

    void operator()(int Nijk, int Nij) { state_ = static_cast<double>(Nijk) / Nij; }

    double state() const { return state_; }

private:
    double state_ = 0.0;

}; // class estimator


template <int N, typename CQE> struct mle_task {
    using data_type = typename CQE::data_type;
    using set_type = uint_type<N>;

    mle_task(const CQE& acqe, const network_struct<N>& aG, network_fitted<N>& afit, std::string& amessage, tbb::spin_mutex& amtx)
        : cqe(acqe), G(aG), fit(afit), message(amessage), mtx(amtx) { }

    void operator()(const tbb::blocked_range<int>& range) const {
        std::vector<estimator> pe(1);

        for (int xi = range.begin(); xi != range.end(); ++xi) {
            bool xi_err = false;

            auto xi_set = set_empty<set_type>();
            xi_set = set_add(xi_set, xi);

            auto pa_vec = as_vector(G[xi].pa);

            int ri = fit.G[xi].range;
            int q = num_states(fit.G, pa_vec);

            fit.P[xi].resize(ri * q);

            int pos = 0;

            for (int j = 0; j < q; ++j) {
                double S = 0.0;
                auto pa_state = var_states<data_type>(fit.G, j, pa_vec);
                for (data_type i = 0; i < ri; ++i) {
                    cqe.apply(xi_set, G[xi].pa, std::vector<data_type>{i}, pa_state, pe);
                    fit.P[xi][pos++] = pe[0].state();
                    S += pe[0].state();
                }

                if (S < 0.999) {
                    if (xi_err == false) {
                        tbb::spin_mutex::scoped_lock lock(mtx);
                        message += std::to_string(xi);
                        xi_err = true;
                    }
                    for (data_type i = 0; i < ri; ++i) fit.P[xi][pos - i - 1] = (1.0 / ri);
                }
            } // for j
        } // for xi
    } // operator()

    const CQE& cqe;
    const network_struct<N>& G;
    network_fitted<N>& fit;

    std::string& message;
    tbb::spin_mutex& mtx;
}; // struct mle_task

template <int N, typename CQE>
inline std::pair<bool, std::string> mle(const CQE& cqe, const network_struct<N>& G, network_fitted<N>& fit) {
    int n = cqe.n();

    if (G.size() != n) return {false, "network and data mismatch"};

    // we allow for G to have .range missing (e.g., after reading SIF)
    fit.G = G;
    fit.P.resize(n);

    for (int xi = 0; xi < n; ++xi) { fit.G[xi].range = cqe.r(xi); }

    using data_type = typename CQE::data_type;
    using set_type = uint_type<N>;

    std::string message = "";
    tbb::spin_mutex mtx;

    mle_task<N, CQE> task(cqe, G, fit, message, mtx);
    tbb::parallel_for(tbb::blocked_range<int>(0, n), task);

    if (message.empty() == false) {
        message = "found uninformative uniform attributes for: " + message;
    }

    return {true, message};
} // mle

#endif // MODEL_FIT_HPP
