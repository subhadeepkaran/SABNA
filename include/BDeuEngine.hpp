/***
 *  $Id$
 **
 *  File: BDeuEngine.hpp
 *  Created: Aug 18, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017-2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef BDEU_ENGINE_HPP
#define BDEU_ENGINE_HPP

#include <chrono>
#include <tuple>
#include <vector>

#include "BDeu.hpp"
#include "ScoringEngine.hpp"
#include "log.hpp"


template <int N, typename CountingQueryEngine>
class BDeuEngine : public ScoringEngine<N> {
public:
    using set_type = uint_type<N>;
    using score_type = BDeu::score_type;

    BDeuEngine(CountingQueryEngine& cqe, BDeu& bdeu, int pa_size) : ScoringEngine<N>(pa_size), cqe_(cqe), bdeu_(bdeu) {
        n_ = cqe_.n();
        m_ = cqe_.m();

        E_ = set_empty<set_type>();
        X_ = set_full<set_type>(n_);

        md_.resize(n_, E_);
        m_max_pa_size__();
    } // BDeuEngine

    int n() const { return n_; }

    int m() const { return m_; }

    bool process(const set_type& pa, set_type& ch, MPSList<N>& mps_list, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer, int l) {
        std::vector<BDeu> vec_bdeu(set_size(ch), bdeu_);
        set_type ch_copy = ch;

        cqe_.apply(ch, pa, vec_bdeu);

        for (int xi = 0, idx = 0; xi < n_; ++xi) {
            if (!in_set(ch, xi)) continue;
            if (!m_extend_insert__(xi, pa, vec_bdeu[idx].score(), mps_list, mps_buffer)) { ch = set_remove(ch, xi); }
            ++idx;
        }

        // setting flag - true iff the xi is pruned due to above
        bool flag = true;
        if (ch == ch_copy) { flag = false; }
        ch = ch & md_[l + 1];

        return flag;
    } // process

    void finalize(MPSList<N>& mps_list) { }


private:
    bool m_extend_insert__(int xi, const set_type& pa, const BDeu::score_type& score, MPSList<N>& mps_list, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer) {
        auto res = mps_list.find(xi, pa);
        if (std::get<0>(score) < res.s) mps_buffer.push_back({xi, {std::get<0>(score), pa}});
        else if ((std::get<1>(score) <= pruning_constant) && (res.s < std::get<2>(score))) return false;
        return true;
    } // m_extend_and_insert__

    void m_max_pa_size__() {
        if ((this->pa_size < 1) || (this->pa_size > n_ - 1)) this->pa_size = n_ - 1;
        for (int i = 0; i <= this->pa_size; ++i) md_[i] = X_;
        Log.info() << "maximal number of parents limited to " << this->pa_size << std::endl;
    } // m_max_pa_size_

    int n_ = -1;
    int m_ = -1;

    BDeu bdeu_;

    CountingQueryEngine& cqe_;

    set_type X_;
    set_type E_;

    int max_md_ = -1;

    std::vector<BDeu> vec_bdeu_;
    std::vector<int> norder_;
    std::vector<set_type> md_;

    // de Campos pruning constant
    // see: Efficient Structure Learning of Bayesian Networks using Constraints by de Campos
    // http://dl.acm.org/citation.cfm?id=1953048.2021027
    const double pruning_constant = 0.8349;

}; // class BDeuEngine

#endif // BDEU_ENGINE_HPP
