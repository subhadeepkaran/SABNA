/***
 *  $Id$
 **
 *  File: tbb.hpp
 *  Created: Apr 02, 2019
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2019 Jaroslaw Zola
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef TBB_HPP
#define TBB_HPP

#include <cstdlib>

#define TBB_PREVIEW_GLOBAL_CONTROL 1

#include <tbb/global_control.h>
#include <tbb/task_scheduler_init.h>


inline int InitTBB() {
    if (const char* env = std::getenv("SABNA_NUM_THREADS")) {
        int nt = std::atoi(env);
        return nt;
    }
    return tbb::task_scheduler_init::default_num_threads();
} // InitTBB

#endif // TBB_HPP
