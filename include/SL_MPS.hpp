/***
 *  $Id$
 **
 *  File: SL_MPS.hpp
 *  Created: June 4, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef SL_MPS_HPP
#define SL_MPS_HPP

#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

#include <tbb/enumerable_thread_specific.h>
#include <tbb/task_group.h>

#include "MPS.hpp"
#include "log.hpp"


template <int N> class SL_MPS : public MPS<N> {
public:
    using set_type = typename MPS<N>::set_type;

    struct Task {
        set_type pa_;
        set_type ch_;
    }; // struct Task


    using shared_mps_t = tbb::enumerable_thread_specific<std::vector<std::pair<int, typename MPS<N>::MPSNode>>>;

    using shared_tasks_t = tbb::enumerable_thread_specific<std::vector<Task>>;


    template <typename Engine> void run(Engine score, int min_dfs) {
        n_ = score.n();
        MPS<N>::init(n_);

        if (min_dfs < 1) min_dfs = n_ + 1;

        E_ = set_empty<set_type>();
        X_ = set_full<set_type>(n_);

        std::vector<Task> cl, nl;
        nl.push_back({E_, X_});

        shared_mps_t mps_buffer;
        shared_tasks_t nl_temp;

        tbb::task_group tg;

        for (int l = 0; l < n_ && !nl.empty() && l <= min_dfs; ++l) {
            auto start = std::chrono::system_clock::now();

            cl.swap(nl);
            nl.clear();

            int cl_size = cl.size();

            if (l < min_dfs) {
                for (int i = 0; i < cl_size; ++i) {
                    tg.run([&, i] {
                               auto& mps = mps_buffer.local();
                               score.process(cl[i].pa_, cl[i].ch_, MPS<N>::mps_list, mps, l);

                               auto& nlt = nl_temp.local();
                               if (!is_emptyset(cl[i].ch_)) m_fill_next_layer__(cl[i].pa_, cl[i].ch_, nlt); });
                }
            } else {
                Log.info() << "switching to DFS mode..." << std::endl;

                for (int i = 0; i < cl_size; ++i) {
                    tg.run([&, i] { auto& mps = mps_buffer.local(); m_dfs__<Engine>(score, cl[i].pa_, cl[i].ch_, mps); });
                }
            }

            tg.wait();

            int nl_size = 0;
            for (auto i = nl_temp.begin(); i != nl_temp.end(); ++i) { nl_size += (*i).size(); }

            nl.reserve(nl_size);

            for (auto i = nl_temp.begin(); i != nl_temp.end(); ++i) {
                auto& nlt = *i;
                std::move(nlt.begin(), nlt.end(), std::back_inserter(nl));
                nlt.clear();
            }

            for (auto i = mps_buffer.begin(); i != mps_buffer.end(); ++i) {
                auto& mpsb = *i;
                for (const auto& xi_mps_node : mpsb) { MPS<N>::mps_list.insert(xi_mps_node.first, xi_mps_node.second.pa, xi_mps_node.second.s); }
                mpsb.clear();
            }

            if (l >= min_dfs) {
                Log.debug() << "sanitizing MPS" << std::endl;
                MPS<N>::mps_list.check_sanitize();
                break;
            }

            std::chrono::duration<double> diff = std::chrono::system_clock::now() - start;
            Log.info() << "processed layer " << l << ", size of next layer " << nl_size << ", time: " << jaz::log::second_to_time(diff.count()) << std::endl;
            Log.debug() << "memory required: " << jaz::log::byte_to_size(nl_size * sizeof(Task)) << std::endl;
        } // for l

        score.finalize(MPS<N>::mps_list);
    } // run

private:
    void m_fill_next_layer__(const set_type& pa, const set_type& ch, std::vector<Task>& nl);
    void m_prune_next_layer__(const std::vector<Task>& B, std::vector<Task>& nl);

    template <typename Engine>
    void m_dfs__(Engine& score, set_type& pa, set_type& ch, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer) {
        struct stack_el {
            int xi_max;
            bool done;
            Task tk;
        };

        int xi_max = n_ - 1;
        for (; xi_max >= 0 && !in_set(pa, xi_max); --xi_max);

        std::vector<stack_el> stack;
        stack.push_back({xi_max, false, {pa, ch}});

        while (!stack.empty()) {
            if (!stack.back().done) {
                score.process(stack.back().tk.pa_, stack.back().tk.ch_, MPS<N>::mps_list, mps_buffer, set_size(pa));
                stack.back().done = true;
            }

            set_type new_ch = set_empty<set_type>();
            bool to_pop = false;
            int xi = ++stack.back().xi_max;
            if (xi >= n_) { to_pop = true; }
            else { new_ch = set_remove(stack.back().tk.ch_, xi); }
            if (to_pop || is_emptyset(new_ch)) { stack.pop_back(); }
            else { stack.push_back({xi, false, {set_add(stack.back().tk.pa_, xi), new_ch}}); }
        }
    } // m_dfs__

    int n_;

    set_type X_;
    set_type E_;

}; // class SL_MPS

template <int N>
inline void SL_MPS<N>::m_fill_next_layer__(const set_type& pa, const set_type& ch, std::vector<Task>& nl) {
    int xi_max = msb(pa);
    for (int xi = xi_max + 1; xi < n_; ++xi) {
        set_type new_ch = set_remove(ch, xi);
        if (!is_emptyset(new_ch)) { nl.push_back({set_add(pa, xi), new_ch}); }
    }
} // m_fill_next_layer__

#endif // SL_MPS_HPP
