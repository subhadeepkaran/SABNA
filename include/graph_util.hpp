/***
 *  $Id$
 **
 *  File: graph_util.hpp
 *  Created: May 13, 2018
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef GRAPH_UTIL_HPP
#define GRAPH_UTIL_HPP

#include <algorithm>
#include <fstream>
#include <numeric>
#include <sstream>
#include <vector>

#include <jaz/iterator.hpp>
#include <jaz/string.hpp>

#include <BVCounter.hpp>
#include <bit_util.hpp>


template <int N>
struct variable {
    int range;
    uint_type<N> pa;
}; // struct variable


template <int N>
using network_struct = std::vector<variable<N>>;

template <int N>
std::ostream& operator<<(std::ostream& os, const network_struct<N> G) {
    int n = G.size();

    std::vector<bool> seen(n, false);
    std::vector<int> check;

    for (int xi = 0; xi < n; ++xi) {
        if (!is_emptyset<N>(G[xi].pa)) {
            auto pa = as_vector(G[xi].pa);
            for (auto p : pa) {
                os << p << " -> " << xi << std::endl;
                seen[p] = true;
            }
        } else check.push_back(xi);
    } // for xi

    for (auto xi : check) if (!seen[xi]) os << xi << " -> " << xi << std::endl;

    return os;
} // operator<<

template <int N>
std::ostream& operator<<(std::ostream& os, const std::vector<uint_type<N>>& net) {
    int n = net.size();

    std::vector<bool> seen(n, false);
    std::vector<int> check;

    for (int xi = 0; xi < n; ++xi) {
        if (!is_emptyset<N>(net[xi])) {
            auto pa = as_vector(net[xi]);
            for (auto p : pa) {
                os << p << " -> " << xi << std::endl;
                seen[p] = true;
            }
        } else check.push_back(xi);
    } // for xi

    for (auto xi : check) if (!seen[xi]) os << xi << " -> " << xi << std::endl;

    return os;
} // operator<<


template <int N>
struct network_fitted {
    using cpt_type = std::vector<double>;

    network_struct<N> G;
    std::vector<cpt_type> P;
}; // struct network_fitted


namespace detail {

  template <typename Iter> inline std::vector<std::string> to_strings(Iter first, Iter last) {
      std::vector<std::string> S;
      for (; first != last; ++first) S.emplace_back(std::to_string(*first));
      return S;
  } // to_strings

  template <typename T> std::vector<std::string> to_strings(const std::vector<T>& list) {
      return to_strings(std::begin(list), std::end(list));
  } // to_strings

} // namespace detail


template <int N>
int num_states(const network_struct<N>& G, const std::vector<int>& pa) {
    int S = 1;
    for (auto x : pa) S *= G[x].range;
    return S;
} // num_states

template <int N>
inline int num_states(const network_struct<N>& G, const uint_type<N>& pa) {
    return num_states(G, as_vector(pa));
} // num_states

template <typename data_type, int N>
std::vector<data_type> var_states(const network_struct<N>& G, int index, const std::vector<int>& pa) {
    if (pa.empty()) return { };
    std::vector<data_type> out(pa.size(), 0);

    for (int i = pa.size() - 1; i != -1; --i) {
        out[i] = index % G[pa[i]].range;
        index = index / G[pa[i]].range;
        if (index == 0) break;
    }

    return out;
} // var_states

template <typename data_type, int N>
std::vector<data_type> var_states(const network_struct<N>& G, int index, const uint_type<N>& pa) {
    return var_state(G, index, as_vector(pa));
} // var_states


template<int N>
bool is_reachable_core(const std::vector<uint_type<N>>& adj, const int xi, const int xj, uint_type<N>& visited) {
    if (in_set(adj[xi], xj)) { return true; }
    const int n = adj.size();
    for (int xk = 0; xk < n; ++xk) {
        if (xk == xi) { continue; }
        if (!in_set(adj[xi], xk)) { continue; }
        if (in_set(visited, xk)) { continue; }
        if (is_reachable_core(adj, xk, xj, visited)) { return true; }
    }
    visited = set_add(visited, xi);
    return false;
} // is_reachable_core

template <int N>
inline bool is_reachable(const std::vector<uint_type<N>>& adj, const int xi, const int xj) {
    using set_type = uint_type<N>;
    set_type visited = set_empty<set_type>();
    return is_reachable_core(adj, xi, xj, visited);
} // is_reachable

// adj is acyclic, and we want to add edge xi->xj
template <int N>
inline bool is_cyclic(const std::vector<uint_type<N>>& adj, int xi, int xj) {
    return is_reachable(adj, xj, xi);
} // is_cyclic


template <int N> inline bool is_cyclic(const std::vector<uint_type<N>>& adj) {
    using set_type = uint_type<N>;

    const int n = adj.size();

    const set_type E = set_empty<set_type>();
    const set_type X = set_full<set_type>(n);

    set_type leaves = E;

    for (int xi = 0; xi < n; ++xi) {
        if (is_emptyset(adj[xi])) { leaves = set_add(leaves, xi); }
    }

    if (is_emptyset(leaves)) { return true; }

    while (X != leaves) {
        set_type new_leaves = E;

        for (int xi = 0; xi < n; ++xi) {
            if (in_set(leaves, xi)) { continue; }
            if (is_emptyset(adj[xi] & ~leaves)) { new_leaves = set_add(new_leaves, xi); }
        }

        if (is_emptyset(new_leaves)) { return true; }
        leaves = leaves | new_leaves;
    } // while

    return false;
} // is_cyclic


template <int N> inline bool too_dense(const std::vector<uint_type<N>>& net, int pa_size) {
    for (const auto& x : net) if (set_size(x) > pa_size) return true;
    return false;
} // too_dense


template <int N>
inline bool read_sif(const std::string& name, int n, network_struct<N>& G) {
    std::ifstream f(name);
    if (!f) { return false; }

    jaz::getline_iterator<> it(f);
    jaz::getline_iterator<> end;

    std::istringstream is;
    int s;

    G = network_struct<N>(n);

    for (; it != end; ++it, ++n) {
        std::string buf = *it;

        is.clear();
        is.str(buf);

        is >> s;
        is >> buf;

        if (!is || (s < 0) || (s >= n)) { return false; }

        // currently we allow only categorical data
        std::istream_iterator<int> iit(is);
        std::istream_iterator<int> iend;

        for (; iit != iend; ++iit) {
            if ((*iit < 0) || (*iit >= n)) { return false; }
            if (*iit != s) { G[*iit].pa = set_add(G[*iit].pa, s); }
        }
    } // for it

    return true;
} // read_sif

template <int N>
inline bool write_sif(const network_struct<N>& G, const std::string& name) {
    std::ofstream of(name);
    if (!of) { return false; }
    of << G;
    of.close();

    return true;
} // write_sif

template <int N>
inline bool write_sif(const network_fitted<N>& net, const std::string& name) {
    return write_sif(net.G, name);
} // write_sif


template <int N>
bool write_bif(const network_fitted<N>& net, const std::string& name) {
    int n = net.G.size();

    std::ofstream of(name);
    if (!of) return false;

    of << "network " << name << " {" << std::endl;
    of << "}\n" << std::endl;

    for (int xi = 0; xi < n; ++xi) {
        of << "variable " << xi << " {" << std::endl;
        of << "  " << "type discrete [ " << net.G[xi].range << " ] { ";
        std::vector<int> temp(net.G[xi].range);
        std::iota(std::begin(temp), std::end(temp), 0);
        auto s = detail::to_strings(temp);
        of << jaz::join(", ", std::begin(s), std::end(s)) << " }" << std::endl;
        of << "}" << std::endl;
    }

    of << "\n";

    for (int xi = 0; xi < n; ++xi) {
        of << "probability ( " << xi;

        int ri = net.G[xi].range;

	if (is_emptyset(net.G[xi].pa)) {
	    of << " ) {" << std::endl;
            of << "  table ";
            auto v = detail::to_strings(net.P[xi]);
            of << jaz::join(", ", std::begin(v), std::end(v));
            of << ";" << std::endl;
	} else {
            auto s = detail::to_strings(as_vector(net.G[xi].pa));
	        of << " | " << jaz::join(", ", std::begin(s), std::end(s)) << " ) {" << std::endl;

            auto pa_vec = as_vector(net.G[xi].pa);
            int q = num_states(net.G, pa_vec);

            for (int i = 0; i < q; ++i) {
                auto v = detail::to_strings(var_states<int>(net.G, i, pa_vec));
                of << "  ( " << jaz::join(", ", std::begin(v), std::end(v)) << " ) ";

                v = detail::to_strings(std::begin(net.P[xi]) + i * ri, std::begin(net.P[xi]) + (i + 1) * ri);
                of << jaz::join(", ", std::begin(v), std::end(v)) << ";" << std::endl;
            }
	}

        of << "}" << std::endl;
    } // for xi

    of.close();

    return true;
} // write_bif

template <int N>
bool write_net(const network_fitted<N>& net, const std::string& name) {
    int n = net.G.size();

    std::ofstream of(name);
    if (!of) return false;

    of << "net " << std::endl << "{" << std::endl;
    of << "}" << std::endl;

    for (int xi = 0; xi < n; ++xi) {
        of << "node " << xi << std::endl <<"{" << std::endl;
        of << "  " << "states = ( ";
        std::vector<int> temp(net.G[xi].range);
        std::iota(std::begin(temp), std::end(temp), 0);
        auto s = detail::to_strings(temp);
        for (auto& x : s) x = "\"" + x + "\"";
        of << jaz::join(" ", std::begin(s), std::end(s)) << " );" << std::endl;
        of << "}" << std::endl;
    }

    of << "\n";

    for (int xi = 0; xi < n; ++xi) {
        of << "potential ( " << xi;

        int ri = net.G[xi].range;

	    if (is_emptyset(net.G[xi].pa)) {
	        of << " )" << std::endl << "{" << std::endl;
            of << "  data = ( ";
            auto v = detail::to_strings(net.P[xi]);
            of << jaz::join(" ", std::begin(v), std::end(v));
            of << " );" << std::endl;
	    } else {
            auto s = detail::to_strings(as_vector(net.G[xi].pa));
	        of << " | " << jaz::join(" ", std::begin(s), std::end(s)) << " ) " << std::endl << "{" << std::endl;

            auto pa_vec = as_vector(net.G[xi].pa);
            int q = num_states(net.G, pa_vec);

            std::vector<std::string> temp;

            for (int i = 0; i < q; ++i) {
                auto v = detail::to_strings(var_states<int>(net.G, i, pa_vec));
                v = detail::to_strings(std::begin(net.P[xi]) + i * ri, std::begin(net.P[xi]) + (i + 1) * ri);
                std::string xx = "(" + jaz::join(" ", std::begin(v), std::end(v)) + ")";
                temp.push_back(xx);
            }

            int bias = 1;
            for (const auto& x : pa_vec) {
                bias *= net.G[x].range;
                int start = 0;
                for (int i = 1; i < temp.size(); ++i) {
                    int idx2 = i * bias - 1;
                    temp[start] = "(" + temp[start];
                    temp[idx2] = temp[idx2] + ")";
                    start = idx2 + 1;
                    if (start >= temp.size()) break;
                }
            }
            
            std::string final_line = "data = ";
            for (const auto& x : temp) { final_line += x; }
            of << final_line << " ;" << std::endl;
	    }

        of << "}" << std::endl;
    } // for xi

    of.close();

    return true;
} // write_net

#endif // GRAPH_UTIL_HPP
