// the largest problem instance SABNA should be able to process
// number of variables SABNA can handle is equal to 64 * SABNA_MAX_INSTANCE
// this variable affects performance and memory footprint
// by default it is set to 8 (i.e. 512 variables max)
// must be greater than 4
#define SABNA_MAX_INSTANCE 8
