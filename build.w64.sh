#!/bin/bash

# This script is designed to work with SABNA <= 2.101

DIR=w64

echo "creating $DIR directory"
mkdir -p $DIR

echo "building for x86_64-w64-mingw32"
echo "sabna-exsl-mpsbuild.exe..."
x86_64-w64-mingw32-g++-win32 -ISABNAtk/include -Iinclude -I. -static -m64 -fopenmp -msse4 -O3 -std=c++14  src/sabna-exsl-mpsbuild.cpp src/log.cpp -o $DIR/sabna-exsl-mpsbuild.exe
echo "sabna-exsl-bfs-ope.exe..."
x86_64-w64-mingw32-g++-posix -ISABNAtk/include -Iinclude -I. -static -m64 -fopenmp -msse4 -O3 -std=c++14  src/sabna-exsl-bfs-ope.cpp src/log.cpp -o $DIR/sabna-exsl-bfs-ope.exe
echo "sabna-pl-mle.exe..."
x86_64-w64-mingw32-g++-posix -ISABNAtk/include -Iinclude -I. -static -m64 -fopenmp -msse4 -O3 -std=c++14  src/sabna-pl-mle.cpp src/log.cpp -o $DIR/sabna-pl-mle.exe
echo "sabna-order2net.exe..."
x86_64-w64-mingw32-g++-posix -ISABNAtk/include -Iinclude -I. -static -m64 -fopenmp -msse4 -O3 -std=c++14  src/sabna-order2net.cpp src/log.cpp -o $DIR/sabna-order2net.exe
echo "done"
