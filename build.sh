#!/bin/bash

if [ -n "$1" ]; then
  DIR="$1"
else
  DIR=`pwd`
fi

mkdir -p build/
rm -rf build/*
cd build/

# Add -DCMAKE_BUILD_TYPE=Debug to debug
cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR

#make -j8 install VERBOSE=1
make -j8 install
