/***
 *  *  $Id$
 *   **
 *    *  File: sabna-exsl-bfs-ope.cpp
 *     *  Created: Aug 22, 2017
 *      *
 *       *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *        *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *         *  Distributed under the MIT License.
 *          *  See accompanying file LICENSE.
 *           */

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <unordered_map>
#include <vector>

#include <cxxopts/cxxopts.hpp>
#include <jaz/math.hpp>

#include <bit_util.hpp>

#include "MPSList.hpp"
#include "config.hpp"
#include "log.hpp"

#include "config.h"


template <int N>
struct bfs_node {
    double score;
    unsigned char path[64 * N];
};

template <int N>
inline void optimal_path_extension(const MPSList<N>& mps_list, const std::vector<std::pair<uint_type<N>, double>>& opt, uint_type<N>& id, bfs_node<N>& node, int& id_size) {
    int n = mps_list.n();
    bool extend = true;

    for (int i = 0; i < n && extend; ++i) {
        extend = false;
        for (int xi = 0; xi < n; ++xi) {
            if (!in_set(id, xi) && is_superset(id, opt[xi].first)) {
                extend = true;
                id = set_add(id, xi);
                node.score += opt[xi].second;
                node.path[id_size++] = xi;
            }
        } // for xi
    } // for i

} // optimal_path_extension

template<int N>
void bfs_core(const MPSList<N>& mps_list, const std::vector<std::pair<uint_type<N>, double>>& opt, const uint_type<N>& source_id, bfs_node<N> source_node, const uint_type<N>& target_id, bfs_node<N>& target_node, int source_id_size) {
    using set_type = uint_type<N>;

    int n = mps_list.n();

    std::vector<std::unordered_map<uint_type<N>, bfs_node<N>, uint_hash>> v_nl(n+1);

    jaz::Binom<int> binom;
    int max_init_reserve = 1 << 20;
    for (int l = source_id_size; l < n; ++l) {
        v_nl[l].reserve( (max_init_reserve) < binom(n, l) ? (max_init_reserve) : binom(n, l));
    }

    v_nl[source_id_size].insert({source_id, source_node});

    uint64_t node_counter = 0;
    for (int l = source_id_size; l <= n; ++l) {
        if (v_nl[l].size() == 0) continue;
        else if ((v_nl[l].size() == 1) && (std::begin(v_nl[l])->first == target_id)) {
                target_node = std::begin(v_nl[l])->second;
                return;
        }

        uint64_t node_l_counter = 0;
        for (const auto& pa_node : v_nl[l]) {
            ++node_l_counter;

            for (int xi = 0; xi < n; ++xi) {
                if (in_set(pa_node.first, xi) || !in_set(target_id, xi)) continue;

                bfs_node<N> ch_node = pa_node.second;
                set_type ch_id = set_add(pa_node.first, xi);
                ch_node.score += mps_list.find(xi, pa_node.first).s;
                int ch_id_size = l;
                ch_node.path[ch_id_size++] = xi;

                optimal_path_extension(mps_list, opt, ch_id, ch_node, ch_id_size);

                auto it = v_nl[ch_id_size].find(ch_id);
                if (it != v_nl[ch_id_size].end()) {
                    if (it->second.score > ch_node.score) it->second = ch_node;
                    else continue;
                }
                else { v_nl[ch_id_size].insert({ch_id, ch_node}); }
            } // for xi

            if (node_l_counter % 100000 == 0) {
                Log.info() << "processing layer " << l << ", nodes explored: " << node_l_counter << std::endl;
            }
        } // for pa_node

        v_nl[l].clear();

        node_counter += node_l_counter;
        if (l < n) Log.info() << "layer " << l << " processed, size of next layer: " << v_nl[l + 1].size() << std::endl;
    }

} // bfs_core

template <int N>
std::pair<bool, std::string> bfs(const MPSList<N>& mps_list, const std::string& out) {
    using set_type = uint_type<N>;

    int n = mps_list.n();
    set_type E = set_empty<set_type>();
    set_type X = set_full<set_type>(n);

    std::vector<std::pair<set_type, double>> opt(n);
    for (int xi = 0; xi < n; ++xi) {
        auto res = mps_list.optimal(xi);
        opt[xi] = {res.pa, res.s};
    }

    std::unordered_map<set_type, std::pair<int, set_type>, uint_hash> ht_path;

    bfs_node<N> source_node;
    source_node.score = 0;
    for (auto& x : source_node.path) { x = 0; }

    bfs_node<N> target_node;
    target_node.score = -1;
    bfs_core(mps_list, opt, E, source_node, X, target_node, 0);

    if (target_node.score == -1) { return {false, "unable to find any optimal structure"}; }

    Log.info() << "found optimal structure with score: " << target_node.score << std::endl;

    if (!out.empty()) {
        std::ofstream of(out);
        for (int i = 0; i < n; ++i) of << static_cast<int>(target_node.path[i]) << " ";
        of.close();
    }

    return {true, ""};
} // bfs

template <int N>
std::pair<bool, std::string> read_search(int n, const std::string& in, const std::string& out) {
    MPSList<N> mps_list;

    Log.info() << "reading MPS" << std::endl;

    auto res = mps_list.read(n, in);
    if (!res.first) return res;

    Log.info() << "MPS has " << mps_list.size() << " parent sets" << std::endl;

    return bfs(mps_list, out);
} // read_search


int main (int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-exsl-bfs-ope");
    Log.debug() << "SABNA_MAX_INSTANCE=" << SABNA_MAX_INSTANCE << std::endl;

    int n;
    std::string mps_name;
    std::string ord_name;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("n,x-size", "number of variables", cxxopts::value<int>(n))
            ("mps-file", "input mps file", cxxopts::value<std::string>(mps_name))
            ("ord-file", "output order file", cxxopts::value<std::string>(ord_name))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("x-size") && opt_res.count("mps-file") && opt_res.count("ord-file")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (n < 0) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "n" + cxxopts::RQUOTE + " option");

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }


    Log.info() << "input: " << mps_name << std::endl;
    Log.info() << "variables: " << n << std::endl;

    auto start = std::chrono::system_clock::now();

    std::pair<bool, std::string> res;

    static_assert(SABNA_MAX_INSTANCE > 4, "SABNA_MAX_INSTANCE must be > 4");

    if (n > 64 * SABNA_MAX_INSTANCE) {
        Log.error() << "too large instance, increase SABNA_MAX_INSTANCE and recompile" << std::endl;
        return -1;
    }

    if (n <= 64) res = read_search<1>(n, mps_name, ord_name);
    else if (n <= 128) res = read_search<2>(n, mps_name, ord_name);
    else if (n <= 256) res = read_search<4>(n, mps_name, ord_name);
    else res = read_search<SABNA_MAX_INSTANCE>(n, mps_name, ord_name);

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    Log.info() << "searching done in " << jaz::log::second_to_time(diff.count()) << std::endl;

    return 0;
} // main
