/***
 *  $Id$
 **
 *  File: sabna-exsl-mpsbuild.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *          Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017-2019 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>

#include <cxxopts/cxxopts.hpp>

#include <BVCounter.hpp>
#include <RadCounter.hpp>

#include "AICEngine.hpp"
#include "BDeuEngine.hpp"
#include "MDLEngine.hpp"
#include "Priors.hpp"
#include "SL_MPS.hpp"
#include "config.hpp"
#include "csv.hpp"
#include "log.hpp"
#include "tbb.hpp"

#include "config.h"


struct Functors {
    AIC aic;
    MDL mdl;
    BDeu bdeu;
}; // struct Functors


template <int N, typename data_type>
std::pair<bool, std::string> build_and_write(int n, int m, std::vector<data_type>& D, const std::string& mps_name, const std::string& priors_name, const std::string& scoring_f, Functors& func, const std::string& cqe_type, int pa_size, int min_dfs) {
    Priors<N> priors;

    if (!priors_name.empty()) {
        Log.warn() << "support for priors is experimental, caution advised!" << std::endl;
        Log.debug() << "reading priors" << std::endl;
        auto res = priors.read(priors_name, n);
        if (!res.first) return res;

        if ((scoring_f == "aic") || (scoring_f == "bdeu")) {
            Log.warn() << scoring_f << " does not support priors yet" << std::endl;
        }
    }

    SL_MPS<N> sl_mps;

    auto start = std::chrono::system_clock::now();

    if (cqe_type == "rad") {
        Log.debug() << "building RadCounter engine" << std::endl;
        RadCounter<N, data_type> rad = create_RadCounter<N, data_type>(n, m, std::begin(D));
        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        if (scoring_f == "aic") sl_mps.run(AICEngine<N, RadCounter<N, data_type>>(rad, func.aic, pa_size), min_dfs);
        else if (scoring_f == "mdl") sl_mps.run(MDLEngine<N, RadCounter<N, data_type>>(rad, func.mdl, priors, pa_size), min_dfs);
        else sl_mps.run(BDeuEngine<N, RadCounter<N, data_type>>(rad, func.bdeu, pa_size), min_dfs);
    } else {
        Log.debug() << "building BVCounter engine" << std::endl;
        BVCounter<N> bvc= create_BVCounter<N>(n, m, std::begin(D));
        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        if (scoring_f == "aic") sl_mps.run(AICEngine<N, BVCounter<N>>(bvc, func.aic, pa_size), min_dfs);
        else if (scoring_f == "mdl") sl_mps.run(MDLEngine<N, BVCounter<N>>(bvc, func.mdl, priors, pa_size), min_dfs);
        else sl_mps.run(BDeuEngine<N, BVCounter<N>>(bvc, func.bdeu, pa_size), min_dfs);
    }

    Log.info() << "writing MPS with " << sl_mps.size() << " parent sets" << std::endl;
    auto res = sl_mps.write(mps_name);

    return res;
} // build_and_write


int main (int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-exsl-mpsbuild");
    Log.debug() << "SABNA_MAX_INSTANCE=" << SABNA_MAX_INSTANCE << std::endl;

    int nt = InitTBB();
    tbb::task_scheduler_init ts(nt);

    Log.debug() << "TBB scheduler limited to " << nt << " threads" << std::endl;

    std::string csv_name;
    std::string mps_name;
    std::string priors_name;
    std::string scoring_f;
    double alpha;
    std::string cqe_type;
    int pa_size;
    int min_dfs;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("csv-file", "input data file, column-wise without header, white space separated", cxxopts::value<std::string>(csv_name))
            ("mps-file", "output mps file", cxxopts::value<std::string>(mps_name))
            ("priors-file", "hard priors file", cxxopts::value<std::string>(priors_name)->default_value(""))
            ("cqe", "counting query engine [rad|bvc]", cxxopts::value<std::string>(cqe_type)->default_value("rad"))
            ("s,score", "scoring function [aic|mdl|bdeu]", cxxopts::value<std::string>(scoring_f)->default_value("mdl"))
            ("a,alpha", "BDeu hyper-parameter", cxxopts::value<double>(alpha)->default_value("1.0"))
            ("l,pa-size", "parent set size limit", cxxopts::value<int>(pa_size)->default_value("-1"))
            ("d,dfs-level", "level to switch to DFS", cxxopts::value<int>(min_dfs)->default_value("-1"))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("csv-file") && opt_res.count("mps-file")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if ((cqe_type != "rad") && (cqe_type != "bvc")) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "cqe" + cxxopts::RQUOTE + " option");
        if ((scoring_f != "aic") && (scoring_f != "mdl") && (scoring_f != "bdeu")) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "score" + cxxopts::RQUOTE + " option");
        if (alpha < 0.0) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "alpha" + cxxopts::RQUOTE + " option");

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }


    using data_type = uint8_t;
    std::vector<data_type> D;

    bool b = false;
    int n = -1;
    int m = -1;

    std::tie(b, n, m) = read_csv(csv_name, D);

    if (b == false) {
        Log.error() << "could not read input data" << std::endl;
        return -1;
    }

    Log.info() << "input: " << csv_name << std::endl;
    Log.info() << "variables: " << n << std::endl;
    Log.info() << "instances: " << m << std::endl;
    Log.info() << "function: " << scoring_f << std::endl;
    Log.info() << "limit: " << pa_size << std::endl;
    Log.info() << "priors: " << priors_name << std::endl;
    Log.info() << "output: " << mps_name << std::endl;

    Log.info() << "searching parent sets" << std::endl;
    auto start = std::chrono::system_clock::now();

    // initialize functors based on provided and default hyper-parameters
    Functors func;

    func.aic = AIC();
    func.mdl = MDL(m);
    func.bdeu = BDeu(alpha);

    static_assert(SABNA_MAX_INSTANCE > 4, "SABNA_MAX_INSTANCE must be > 4");

    if (n > 64 * SABNA_MAX_INSTANCE) {
        Log.error() << "too large instance, increase SABNA_MAX_INSTANCE and recompile" << std::endl;
        return -1;
    }

    std::pair<bool, std::string> res;

    if (n > 256) res = build_and_write<SABNA_MAX_INSTANCE, data_type>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else if (n > 128) res = build_and_write<4, data_type>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else if (n > 64) res = build_and_write<2, data_type>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else res = build_and_write<1, data_type>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    Log.info() << "searching done in " << jaz::log::second_to_time(diff.count()) << std::endl;

    return 0;
} // main
