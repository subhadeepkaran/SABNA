/***
 *  $Id$
 **
 *  File: sabna-hsl-pbil.cpp
 *  Created: Jul 25, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <numeric>
#include <random>
#include <vector>

#include <cxxopts/cxxopts.hpp>
#include <jaz/math.hpp>
#include <jaz/sffun.h>

#include <BVCounter.hpp>
#include <RadCounter.hpp>

#include "AIC.hpp"
#include "BDeu.hpp"
#include "MDL.hpp"
#include "Priors.hpp"
#include "config.hpp"
#include "csv.hpp"
#include "eval.hpp"
#include "graph_util.hpp"
#include "limits.hpp"
#include "log.hpp"
#include "model_fit.hpp"

#include "config.h"



struct IOConfig {
    std::string csv_name;
    std::string priors_name;
    std::string net_name;
    std::string type;
}; // struct IOConfig

template <typename Data = uint8_t>
struct BNConfig {
    using data_type = Data;
    int n;
    int m;
    std::vector<data_type> D;
    std::string cqe_type;
    std::string scoring_f;
    double alpha;
    int pa_size;
}; // struct BNConfig

struct PbILConfig {
    int seed;
    int ps;
    double lr;
    double mf;
    double mr;
}; // struct PbILConfig


inline int ltsf(int i, int j) {
    if (i < j) return ltsf0(j, i);
    return ltsf0(i, j);
} // ltsf

class PbIL {
public:
    using p_type = double;

    PbIL(int seed, int n, int ps, p_type lr, p_type mf, p_type mr)
        : n_(n), ps_(ps), lr_(lr), mf_(mf), mr_(mr), prob_(n * n, 0.5), dep_(jaz::nc2(n), 0), order_(n) {

        if (seed == -1) {
            std::random_device rd;
            gen_ = std::mt19937(rd());
        } else gen_ = std::mt19937(seed);

        randb_ = std::binomial_distribution<>();
        rand0m_ = std::uniform_int_distribution<>(0, n * n - 1);
        rand01_ = std::uniform_real_distribution<p_type>(0.0, 1.0);

        std::iota(std::begin(order_), std::end(order_), 0);
    } // PbIL

    template <int N> void init(const Priors<N>& prior) {
        Log.debug() << "solver initialized, ps=" << ps_ << ", lr=" << lr_ << ", mf=" << mf_ << ", mr=" << mr_ << std::endl;
    } // init

    template <int N> bool sample(std::vector<uint_type<N>>& net) {
        std::memset(net.data(), 0, net.size() * sizeof(uint_type<N>));
        std::vector<uint_type<N>> adj(net);

        std::memset(dep_.data(), 0, dep_.size() * sizeof(char));

        for (int i = 0; i < n_; ++i) {
            for (int j = 0; j < n_; ++j) {
                if (i != j) {
                    auto pos = i * n_ + j;
                    auto deppos = ltsf(i, j);

                    if (!dep_[deppos]) {
                        if (rand01_(gen_) < prob_[pos]) {
                            net[j] = set_add(net[j], i);
                            adj[i] = set_add(adj[i], j);
                            dep_[deppos] = 1;
                        }
                    }
                }
            } // for j
        } // for i

        if (is_cyclic(adj)) {
            // break cycle by Skiena's heuristic
            std::shuffle(std::begin(order_), std::end(order_), gen_);

            for (int xi = 0; xi < n_; ++xi) {
                for (int xj = 0; xj < n_; ++xj) {
                    if (in_set(net[xi], xj)) {
                        if (order_[xi] < order_[xj]) net[xi] = set_remove(net[xi], xj);
                    }
                }
            }
        } // if is_cyclic

        if (count_++ < ps_) return true;
        count_ = 0;

        return false;
    } // sample

    template <int N> bool update(const std::vector<uint_type<N>>& best) {
        for (auto& x : prob_) x -= (x * lr_);

        for (int j = 0; j < n_; ++j) {
            for (int i = 0; i < n_; ++i) {
                if (in_set(best[j], i)) {
                    auto pos = i * n_ + j;
                    prob_[pos] += lr_;
                }
            } // for i
        } // for j

        // add some randomness
        int m = prob_.size();

        if (mf_ * m < 1) {
            for (auto& x : prob_) {
                if (rand01_(gen_) < mf_) x = x * (1.0 - mr_) + randb_(gen_) * mr_;
            }
        } else {
            for (int i = 0; i < mf_ * m; ++i) {
                auto pos = rand0m_(gen_);
                prob_[pos] = prob_[pos] * (1.0 - mr_) + randb_(gen_) * mr_;
            }
        } // if

        return true;
    } // update

    std::ostream& write(std::ostream& os) const {
        for (int i = 0; i < n_; ++i) {
            for (int j = 0; j < n_; ++j) {
                auto pos = i * n_ + j;
                if ((i != j) && (prob_[pos] > 0.000001)) {
                    os << i << " " << j << " " << prob_[pos] << "\n";
                }
            } // for j
        } // for i

        return os;
    } // write

private:
    int n_;

    int ps_;

    p_type lr_;
    p_type mf_;
    p_type mr_;

    std::vector<p_type> prob_;
    std::vector<char> dep_;

    std::mt19937 gen_;
    std::vector<int> order_;

    std::binomial_distribution<> randb_;
    std::uniform_int_distribution<> rand0m_;
    std::uniform_real_distribution<p_type> rand01_;

    int count_ = 0;

}; // class PbIL



template <int N, typename ScoreFunction, typename CQE, typename data_type>
network_struct<N> search(BNConfig<data_type>& bn_conf, CQE& cqe, ScoreFunction F, PbIL& pbil) {
    network_struct<N> net;

    std::vector<uint_type<N>> net_final;
    double score_final = SABNA_DBL_INFTY;

    std::vector<uint_type<N>> net_curr(bn_conf.n);
    pbil.sample(net_curr);

    std::vector<uint_type<N>> net_best;

    Eval<N> eval(bn_conf.n);

    int ntry = 0;
    int iter = 0;

    do {
        if ((iter % 10) == 0) Log.info() << "processing generation " << iter << std::endl;
        iter++;

        double score_best = eval(net_curr, cqe, F);
        net_best = net_curr;

        while (pbil.sample(net_curr)) {
            auto S = eval(net_curr, cqe, F);
            Log.debug() << "current score: " << S << std::endl;
            if (S < score_best) {
                net_best = net_curr;
                score_best = S;
            }
        }

        pbil.update(net_best);
        ntry++;

        if (score_best < score_final) {
            net_final = net_best;
            score_final = score_best;
            ntry = 0;
            Log.info() << "score improved: " << score_final << std::endl;
        }

    } while (ntry < 100);

    Log.info() << "found network with score: " << score_final << std::endl;

    // convert to net format
    net.resize(net_final.size());
    for (int i = 0; i < net_final.size(); ++i) {
        net[i].pa = net_final[i];
        net[i].range = cqe.r(i);
    }

    return net;
} // search

template <int N, typename CQE, typename data_type>
network_struct<N> dispatch_score(BNConfig<data_type>& bn_conf, CQE& cqe, PbIL& pbil) {
    network_struct<N> net;

    if (bn_conf.scoring_f == "aic") net = search<N>(bn_conf, cqe, AIC(), pbil);
    if (bn_conf.scoring_f == "bdeu") net = search<N>(bn_conf, cqe, BDeu(bn_conf.alpha), pbil);
    if (bn_conf.scoring_f == "mdl") net = search<N>(bn_conf, cqe, MDL(bn_conf.m), pbil);

    return net;
} // dispatch_score


template <int N, typename CQE>
std::pair<bool, std::string> write_result(IOConfig& io_conf, CQE& cqe, const network_struct<N>& G, const PbIL& pbil) {
    if (io_conf.type == "bif") {
        Log.info() << "estimating parameters via MLE" << std::endl;
        network_fitted<N> net;
        auto res = mle(cqe, G, net);
        if (!res.first) return {false, res.second};
        if (!res.second.empty()) Log.warn() << res.second << std::endl;
        if (!write_bif(net, io_conf.net_name)) return {false, "unable to create output file"};
    } else {
        if (!write_sif(G, io_conf.net_name)) return {false, "unable to create output file"};
    }

    // dump probability
    std::ofstream of(io_conf.net_name + ".prob");
    if (!of) return {false, "unable to create output file"};
    pbil.write(of);
    of.close();

    return {true, ""};
} // write_result


template <int N, typename data_type = uint8_t>
std::pair<bool, std::string> dispatch(IOConfig& io_conf, BNConfig<data_type>& bn_conf, PbILConfig& pbil_conf) {
    Priors<N> priors;

    if (!io_conf.priors_name.empty()) {
        Log.warn() << "support for priors is experimental, caution advised!" << std::endl;
        Log.debug() << "reading priors" << std::endl;
        auto res = priors.read(io_conf.priors_name, bn_conf.n);
        if (!res.first) return res;
    }

    PbIL pbil(pbil_conf.seed, bn_conf.n, pbil_conf.ps, pbil_conf.lr, pbil_conf.mf, pbil_conf.mr);
    pbil.init(priors);

    network_struct<N> G;

    if (bn_conf.cqe_type == "rad") {
        Log.debug() << "building RadCounter engine" << std::endl;
        RadCounter<N> rad = create_RadCounter<N>(bn_conf.n, bn_conf.m, std::begin(bn_conf.D));
        G = dispatch_score<N>(bn_conf, rad, pbil);
        return write_result<N>(io_conf, rad, G, pbil);
    } else {
        Log.debug() << "building BVCounter engine" << std::endl;
        BVCounter<N> bvc = create_BVCounter<N>(bn_conf.n, bn_conf.m, std::begin(bn_conf.D));
        G = dispatch_score<N>(bn_conf, bvc, pbil);
        return write_result<N>(io_conf, bvc, G, pbil);
    }


    return {true, ""};
} // dispatch


int main (int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-hsl-pbil");
    Log.debug() << "SABNA_MAX_INSTANCE=" << SABNA_MAX_INSTANCE << std::endl;

    IOConfig io_conf;
    BNConfig<> bn_conf;
    PbILConfig pbil_conf;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("csv-file", "input data file, column-wise without header, white space separated", cxxopts::value<std::string>(io_conf.csv_name))
            ("priors-file", "hard priors file", cxxopts::value<std::string>(io_conf.priors_name)->default_value(""))
            ("net-name", "output network name", cxxopts::value<std::string>(io_conf.net_name))
            ("format", "output network format [bif|sif]", cxxopts::value<std::string>(io_conf.type)->default_value("bif"))
            ("cqe", "counting query engine [rad|bvc]", cxxopts::value<std::string>(bn_conf.cqe_type)->default_value("rad"))
            ("s,score", "scoring function [aic|mdl|bdeu]", cxxopts::value<std::string>(bn_conf.scoring_f)->default_value("mdl"))
            ("a,alpha", "BDeu hyper-parameter", cxxopts::value<double>(bn_conf.alpha)->default_value("1.0"))
            ("l,pa-size", "parent set size limit", cxxopts::value<int>(bn_conf.pa_size)->default_value("-1"))
            ("seed", "random seed", cxxopts::value<int>(pbil_conf.seed)->default_value("-1"))
            ("ps", "population size", cxxopts::value<int>(pbil_conf.ps)->default_value("250"))
            ("lr", "learning rate", cxxopts::value<double>(pbil_conf.lr)->default_value("0.05"))
            ("mf", "mutation frequency", cxxopts::value<double>(pbil_conf.mf)->default_value("0.01"))
            ("mr", "mutation rate", cxxopts::value<double>(pbil_conf.mr)->default_value("0.01"))
            ("help", "print this help");

        auto opt_res = options.parse(argc, argv);

        if (!opt_res.count("csv-file") || !opt_res.count("net-name")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if ((bn_conf.cqe_type != "rad") && (bn_conf.cqe_type != "bvc")) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "cqe" + cxxopts::RQUOTE + " option");
        if ((bn_conf.scoring_f != "aic") && (bn_conf.scoring_f != "mdl") && (bn_conf.scoring_f != "bdeu")) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "score" + cxxopts::RQUOTE + " option");
        if (bn_conf.alpha < 0.0) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "alpha" + cxxopts::RQUOTE + " option");

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }


    bool b = false;
    std::tie(b, bn_conf.n, bn_conf.m) = read_csv(io_conf.csv_name, bn_conf.D);

    if (b == false) {
        Log.error() << "could not read input data" << std::endl;
        return -1;
    }

    static_assert(SABNA_MAX_INSTANCE > 4, "SABNA_MAX_INSTANCE must be > 4");

    if (bn_conf.n > 64 * SABNA_MAX_INSTANCE) {
        Log.error() << "too large instance, increase SABNA_MAX_INSTANCE and recompile" << std::endl;
        return -1;
    }

    Log.info() << "input: " << io_conf.csv_name << std::endl;
    Log.info() << "variables: " << bn_conf.n << std::endl;
    Log.info() << "instances: " << bn_conf.m << std::endl;
    Log.info() << "function: " << bn_conf.scoring_f << std::endl;
    Log.info() << "limit: " << bn_conf.pa_size << std::endl;
    Log.info() << "priors: " << io_conf.priors_name << std::endl;
    Log.info() << "output: " << io_conf.net_name << std::endl;

    Log.info() << "searching model" << std::endl;
    auto start = std::chrono::system_clock::now();

    std::pair<bool, std::string> res;

    if (bn_conf.n > 256) res = dispatch<SABNA_MAX_INSTANCE>(io_conf, bn_conf, pbil_conf);
    else if (bn_conf.n > 128) res = dispatch<4>(io_conf, bn_conf, pbil_conf);
    else if (bn_conf.n > 64) res = dispatch<2>(io_conf, bn_conf, pbil_conf);
    else res = dispatch<1>(io_conf, bn_conf, pbil_conf);

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    Log.info() << "searching done in " << jaz::log::second_to_time(diff.count()) << std::endl;

    return 0;
} // main
