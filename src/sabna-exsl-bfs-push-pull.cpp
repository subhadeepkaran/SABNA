/***
 *  $Id$
 **
 *  File: sabna-exsl-bfs-ope.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <stdio.h>
#include <unordered_map>
#include <vector>

#include <tbb/concurrent_hash_map.h>
#include <tbb/task_group.h>

#include <cxxopts/cxxopts.hpp>
#include <jaz/math.hpp>

#include <bit_util.hpp>

#include "MPSList.hpp"
#include "config.hpp"
#include "log.hpp"
#include "tbb.hpp"

#include "config.h"

template<int N>
using set_type = uint_type<N>;


template <int N> struct bfs_node {
    uint64_t lid;
    double score;
    uint64_t offset;
    uint8_t path[N * 64];
}; // struct bfs_node


template<int N>
bool operator<(const bfs_node<N>& lhs, const bfs_node<N>& rhs){ return lhs.lid < rhs.lid; }


template<int N>
inline set_type<N> path_to_set(const uint8_t path[], int level) { return as_set<set_type<N>>(path, path + level); } 


int msb_path(const uint8_t path[], int level) {
    int xi_msb = -1;
    for (int i = 0; i < level; ++i) { xi_msb = xi_msb > path[i] ? xi_msb : path[i]; }
    return xi_msb;
} //msb_path


template <int N>
inline void optimal_path_extension(const MPSList<N>& mps_list, const std::vector<std::pair<set_type<N>, double>>& opt, set_type<N>& id, bfs_node<N>& node, int& id_size) {
    int n = mps_list.n();
    bool extend = true;

    for (int i = 0; i < n && extend; ++i) {
        extend = false;
        for (int xi = 0; xi < n; ++xi) {
            if (!in_set(id, xi) && is_superset(id, opt[xi].first)) {
                extend = true;
                id = set_add(id, xi);
                node.score += opt[xi].second;
                node.path[id_size++] = xi;
            } // if
        } // for xi
    } // for i

} // optimal_path_extension


template<int N>
uint64_t lookup_push_layer(const std::vector<bfs_node<N>>& push_layer, uint64_t idx_pl, int l) {
    uint64_t idx = -1;
    idx = std::lower_bound(push_layer.begin(), push_layer.end(), bfs_node<N>{idx_pl}) - push_layer.begin();
    if (push_layer[idx].lid == idx_pl) { return idx; }
    return -1;
} // lookup_push_layer


template<int N>
void pull_update(const MPSList<N>& mps_list, const std::vector<bfs_node<N>>& push_layer, const set_type<N>& target_id, 
                const bfs_node<N>& pa_node, const int xi, const set_type<N>& ch_id, const int l, bfs_node<N>& ch_node) {
     const int n = mps_list.n();
    jaz::CombIndex<uint64_t> comb;

     for (int xj = 0; xj < n; ++xj) {
        if ((xi == xj) || !in_set(ch_id, xj) || !in_set(target_id, xj)) continue;
        const set_type<N> npa_id = set_remove(ch_id, xj);
        //const uint64_t npa_lid = lexicographic_index(npa_id, n);
        const uint64_t npa_lid = comb(n, as_vector(npa_id));
        const uint64_t idx_pl = lookup_push_layer(push_layer, npa_lid, l);

        if (idx_pl == -1) { continue; }

        const bfs_node<N>& npa_node = push_layer[idx_pl];
        const double cand_score = npa_node.score + mps_list.find(xj, npa_id).s;
        if (cand_score < ch_node.score) {
            ch_node.score = cand_score;
            std::memcpy(ch_node.path, npa_node.path, l);
            ch_node.path[l] = xj; 
        }
    } // for xj
} // pull_update


template <int N>
using TasksTable = tbb::concurrent_hash_map<set_type<N>, bfs_node<N>, tbb_uint_hash>;


template <int N> void bfs_task(const MPSList<N>& mps_list, const std::vector<std::pair<set_type<N>, double>>& opt, const set_type<N>& target_id,
                               const bfs_node<N>& pa_node, int l, std::vector<bfs_node<N>>& push_layer, std::vector<bfs_node<N>>& pull_layer,
                               std::vector<TasksTable<N>>& ope_layer) {
    const int n = mps_list.n();
    const set_type<N> pa_id = path_to_set<N>(pa_node.path, l);
    const int xi_msb = msb(pa_id);
    jaz::CombIndex<uint64_t> comb;

    int nd_counter = 0;
    for (int xi = xi_msb + 1; xi < n; ++xi) {
        if (in_set(pa_id, xi) || !in_set(target_id, xi)) continue;
        
        bfs_node<N> ch_node = pa_node;
        ch_node.score += mps_list.find(xi, pa_id).s;
        set_type<N> ch_id = set_add(pa_id, xi);
        ch_node.path[l] = xi;
        int ch_id_size = set_size(ch_id);
        
        pull_update(mps_list, push_layer, target_id, pa_node, xi, ch_id, l, ch_node);
        optimal_path_extension(mps_list, opt, ch_id, ch_node, ch_id_size);
        ch_node.lid = comb(n, as_vector(ch_id));
    
        bool exists_in_ope = false;
        {
            typename TasksTable<N>::accessor acc;
            
            exists_in_ope = ope_layer[ch_id_size].find(acc, ch_id);
            if (exists_in_ope) { 
                if (ch_id_size == l + 1) {
                    if (acc->second.score < ch_node.score) { pull_layer[pa_node.offset + (nd_counter++)] = acc->second; }
                    else { pull_layer[pa_node.offset + (nd_counter++)] = ch_node; }
                    ope_layer[ch_id_size].erase(acc); 
                }
                else { if (acc->second.score > ch_node.score) { acc->second = ch_node; } }               
            }
            else { 
                if (ch_id_size == l + 1) { pull_layer[pa_node.offset + (nd_counter++)] = ch_node; }
                else { ope_layer[ch_id_size].insert({ch_id, ch_node}); }
            } // if-else
        } 
    } // for xi
} // bfs_task


template <int N>
bool is_ope_node(const std::vector<std::pair<set_type<N>, double>>& opt, const set_type<N>& id) {
    const int n = opt.size();
    for (int xi = 0; xi < n; ++xi) {
        if (!in_set(id, xi) && is_superset(id, opt[xi].first)) { return true; }
    } // for xi
    return false;
} // optimal_path_extension


template<int N>
uint64_t populate_offset(int l, const std::vector<std::pair<set_type<N>, double>>& opt, std::vector<bfs_node<N>>& push_layer){
    const int n = opt.size();    

    uint64_t offset = 0;
    for (auto& nd : push_layer) {
        const set_type<N> p_id = path_to_set<N>(nd.path, l);
        int xi_msb = msb_path(nd.path, l);

        nd.offset = offset;
        for (int xi = xi_msb + 1; xi < n; ++xi) {
            const set_type<N> n_id = set_add(p_id, xi);
            if (!is_ope_node(opt, n_id)) { ++offset; }
        } // for xi
    } // for nd
    return offset;
} // populate_offset


template<int N>
bool shortest_path_found(const std::vector<bfs_node<N>>& layer, const set_type<N>& target_id, int l, bfs_node<N>& target_node) {
    if (layer.size() == 1) {
        const auto id = path_to_set<N>(layer[0].path, l);
        if (id == target_id) {
            target_node = layer[0];
            return true;
        } // if id == target_id
    } // if |layer| == 1
    return false;
} // is_shortest_path


template<int N>
void merge_ope_layer_push_layer(const TasksTable<N>& ope_layer, std::vector<bfs_node<N>>& push_layer) {
    for (const auto& nd : ope_layer) { push_layer.push_back(nd.second); }
    std::sort(push_layer.begin(), push_layer.end());
}// merge_ope_layer_push_layer


template <int N>
void bfs_core(const MPSList<N>& mps_list, const std::vector<std::pair<set_type<N>, double>>& opt, const set_type<N>& source_id, bfs_node<N> source_node, 
            const set_type<N>& target_id, bfs_node<N>& target_node, int source_id_size) {
    int n = mps_list.n();

    std::vector<std::vector<bfs_node<N>>> layer(n + 1);
    std::vector<TasksTable<N>> ope_nodes(n + 1);
    
    jaz::Binom<long long int> binom;
    int max_init_reserve = 1 << 20;

    Log.info() << "allocating memory, layer size limit: " << max_init_reserve << std::endl;

    for (int l = source_id_size; l < n; ++l) {
        auto sz = (max_init_reserve < binom(n, l)) ? max_init_reserve : binom(n, l);
        Log.debug() << "task table preallocation, layer " << l << ", size " << sz << std::endl;
        layer[l].resize(sz);
    } // for l

    Log.info() << "starting search..." << std::endl;

    layer[source_id_size][0] = source_node;
    for (int l = source_id_size; l <= n && l <= 20; ++l) {
        merge_ope_layer_push_layer(ope_nodes[l], layer[l]);
        if (layer[l].size() == 0) { continue; }
        else if (shortest_path_found(layer[l], target_id, l, target_node)) { return; }
 
        //tbb::task_group tg;

        uint64_t pull_layer_size = populate_offset(l, opt, layer[l]);
        pull_layer_size;
       
        layer[l+1].resize(pull_layer_size);
        for (const auto& pa_node : layer[l]) {
            bfs_task<N>(mps_list, opt, target_id, pa_node, l, layer[l], layer[l + 1], ope_nodes);
        } // for pa_node

        //tg.wait();
    
        layer[l].clear();
        if (l < n) Log.info() << "layer " << l << " processed, size of next layer: " << layer[l + 1].size() << std::endl;
    } // for l

} // bfs_core


template <int N>
std::pair<bool, std::string> bfs(const MPSList<N>& mps_list, const std::string& out) {
    int n = mps_list.n();
    const set_type<N> E = set_empty<set_type<N>>();
    const set_type<N> X = set_full<set_type<N>>(n);

    std::vector<std::pair<set_type<N>, double>> opt(n);
    for (int xi = 0; xi < n; ++xi) {
        auto res = mps_list.optimal(xi);
        opt[xi] = {res.pa, res.s};
    }

    bfs_node<N> source_node;
    source_node.lid = 0;
    source_node.offset = 0;
    source_node.score = 0;
    for (auto& x : source_node.path) { x = 0; }

    bfs_node<N> target_node;
    target_node.lid = 0;
    target_node.offset = 0;
    target_node.score = -1;
    bfs_core(mps_list, opt, E, source_node, X, target_node, set_size(E));

    if (target_node.score == -1) { return {false, "unable to find any optimal structure"}; }

    Log.info() << "found optimal structure with score: " << target_node.score << std::endl;

    if (!out.empty()) {
        std::ofstream of(out);
        for (int i = 0; i < n; ++i) of << static_cast<int>(target_node.path[i]) << " ";
        of.close();
    }

    return {true, ""};
} // bfs

template <int N>
std::pair<bool, std::string> read_search(int n, const std::string& in, const std::string& out) {
    MPSList<N> mps_list;

    Log.info() << "reading MPS" << std::endl;

    auto res = mps_list.read(n, in);
    if (!res.first) return res;

    Log.info() << "MPS has " << mps_list.size() << " parent sets" << std::endl;

    return bfs(mps_list, out);
} // read_search


int main (int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-exsl-bfs-ope");
    Log.debug() << "SABNA_MAX_INSTANCE=" << SABNA_MAX_INSTANCE << std::endl;

    int nt = InitTBB();
    tbb::task_scheduler_init ts(nt);

    Log.debug() << "TBB scheduler limited to " << nt << " threads" << std::endl;

    int n;
    std::string mps_name;
    std::string ord_name;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("n,x-size", "number of variables", cxxopts::value<int>(n))
            ("mps-file", "input mps file", cxxopts::value<std::string>(mps_name))
            ("ord-file", "output order file", cxxopts::value<std::string>(ord_name))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("x-size") && opt_res.count("mps-file") && opt_res.count("ord-file")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (n < 0) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "n" + cxxopts::RQUOTE + " option");

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }


    Log.info() << "input: " << mps_name << std::endl;
    Log.info() << "variables: " << n << std::endl;

    auto start = std::chrono::system_clock::now();

    std::pair<bool, std::string> res;

    static_assert(SABNA_MAX_INSTANCE > 4, "SABNA_MAX_INSTANCE must be > 4");

    if (n > 64 * SABNA_MAX_INSTANCE) {
        Log.error() << "too large instance, increase SABNA_MAX_INSTANCE and recompile" << std::endl;
        return -1;
    }

    if (n <= 64) res = read_search<1>(n, mps_name, ord_name);
    else if (n <= 128) res = read_search<2>(n, mps_name, ord_name);
    else if (n <= 256) res = read_search<4>(n, mps_name, ord_name);
    else res = read_search<SABNA_MAX_INSTANCE>(n, mps_name, ord_name);

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    Log.info() << "searching done in " << jaz::log::second_to_time(diff.count()) << std::endl;

    return 0;
} // main
