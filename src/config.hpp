#ifndef CONFIG_HPP
#define CONFIG_HPP

#define SABNA_VERSION "2.200"

inline void score_message(const std::string& name = "") {
    std::cout << "SABNA: Scalable Accelerated Bayesian Network Analytics, version " << SABNA_VERSION << std::endl;
    std::cout << "Copyright (c) 2016-2019 SCoRe Group http://www.score-group.org/" << std::endl;
    std::cout << "https://gitlab.com/SCoRe-Group/SABNA-Release" << std::endl;
    std::cout << name << std::endl;
} // score_message

#endif // CONFIG_HPP
