/***
 *  $Id$
 **
 *  File: sabna-hsl-hc.cpp
 *  Created: Jul 25, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <random>
#include <vector>

#include <cxxopts/cxxopts.hpp>
#include <jaz/math.hpp>

#include <BVCounter.hpp>
#include <RadCounter.hpp>

#include "AIC.hpp"
#include "BDeu.hpp"
#include "MDL.hpp"
#include "Priors.hpp"
#include "config.hpp"
#include "csv.hpp"
#include "eval.hpp"
#include "graph_util.hpp"
#include "limits.hpp"
#include "log.hpp"
#include "model_fit.hpp"

#include "config.h"



struct IOConfig {
    std::string csv_name;
    std::string priors_name;
    std::string net_name;
    std::string type;
}; // struct IOConfig

template <typename Data = uint8_t>
struct BNConfig {
    using data_type = Data;
    int n;
    int m;
    std::vector<data_type> D;
    std::string cqe_type;
    std::string scoring_f;
    double alpha;
    int pa_size;
}; // struct BNConfig

struct HCConfig {
    int seed;
}; // struct HCConfig


template <int N, typename ScoreFunction, typename CQE, typename data_type>
network_struct<N> search(BNConfig<data_type>& bn_conf, CQE& cqe, ScoreFunction F) {
    int n = bn_conf.n;

    std::vector<uint_type<N>> curr(n);
    Eval<N> eval(n);

    


    // convert to net format
    network_struct<N> net(n);

    for (int i = 0; i < n; ++i) {
        net[i].pa = curr[i];
        net[i].range = cqe.r(i);
    }

    return net;
} // search


template <int N, typename CQE, typename data_type>
network_struct<N> dispatch_score(BNConfig<data_type>& bn_conf, HCConfig& hc_conf, Priors<N>& priors, CQE& cqe) {
    network_struct<N> net;

    if (bn_conf.scoring_f == "aic") net = search<N>(bn_conf, cqe, AIC());
    if (bn_conf.scoring_f == "bdeu") net = search<N>(bn_conf, cqe, BDeu(bn_conf.alpha));
    if (bn_conf.scoring_f == "mdl") net = search<N>(bn_conf, cqe, MDL(bn_conf.m));

    return net;
} // dispatch_score


template <int N, typename CQE>
std::pair<bool, std::string> write_result(IOConfig& io_conf, CQE& cqe, const network_struct<N>& G) {
    if (io_conf.type == "bif") {
        Log.info() << "estimating parameters via MLE" << std::endl;
        network_fitted<N> net;
        auto res = mle(cqe, G, net);
        if (!res.first) return {false, res.second};
        if (!res.second.empty()) Log.warn() << res.second << std::endl;
        if (!write_bif(net, io_conf.net_name)) return {false, "unable to create output file"};
    } else {
        if (!write_sif(G, io_conf.net_name)) return {false, "unable to create output file"};
    }

    return {true, ""};
} // write_result


template <int N, typename data_type = uint8_t>
std::pair<bool, std::string> dispatch(IOConfig& io_conf, BNConfig<data_type>& bn_conf, HCConfig& hc_conf) {
    Priors<N> priors;

    if (!io_conf.priors_name.empty()) {
        Log.warn() << "support for priors is experimental, caution advised!" << std::endl;
        Log.debug() << "reading priors" << std::endl;
        auto res = priors.read(io_conf.priors_name, bn_conf.n);
        if (!res.first) return res;
    }

    network_struct<N> G;

    if (bn_conf.cqe_type == "rad") {
        Log.debug() << "building RadCounter engine" << std::endl;
        RadCounter<N> rad = create_RadCounter<N>(bn_conf.n, bn_conf.m, std::begin(bn_conf.D));
        G = dispatch_score<N>(bn_conf, hc_conf, priors, rad);
        return write_result<N>(io_conf, rad, G);
    } else {
        Log.debug() << "building BVCounter engine" << std::endl;
        BVCounter<N> bvc = create_BVCounter<N>(bn_conf.n, bn_conf.m, std::begin(bn_conf.D));
        G = dispatch_score<N>(bn_conf, hc_conf, priors, bvc);
        return write_result<N>(io_conf, bvc, G);
    }


    return {true, ""};
} // dispatch


int main (int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-hsl-hc");
    Log.debug() << "SABNA_MAX_INSTANCE=" << SABNA_MAX_INSTANCE << std::endl;

    IOConfig io_conf;
    BNConfig<> bn_conf;
    HCConfig hc_conf;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("csv-file", "input data file, column-wise without header, white space separated", cxxopts::value<std::string>(io_conf.csv_name))
            ("priors-file", "hard priors file", cxxopts::value<std::string>(io_conf.priors_name)->default_value(""))
            ("net-name", "output network name", cxxopts::value<std::string>(io_conf.net_name))
            ("format", "output network format [bif|sif]", cxxopts::value<std::string>(io_conf.type)->default_value("bif"))
            ("cqe", "counting query engine [rad|bvc]", cxxopts::value<std::string>(bn_conf.cqe_type)->default_value("rad"))
            ("s,score", "scoring function [aic|mdl|bdeu]", cxxopts::value<std::string>(bn_conf.scoring_f)->default_value("mdl"))
            ("a,alpha", "BDeu hyper-parameter", cxxopts::value<double>(bn_conf.alpha)->default_value("1.0"))
            ("l,pa-size", "parent set size limit", cxxopts::value<int>(bn_conf.pa_size)->default_value("-1"))
            ("seed", "random seed", cxxopts::value<int>(hc_conf.seed)->default_value("-1"))
            ("help", "print this help");

        auto opt_res = options.parse(argc, argv);

        if (!opt_res.count("csv-file") || !opt_res.count("net-name")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if ((bn_conf.cqe_type != "rad") && (bn_conf.cqe_type != "bvc")) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "cqe" + cxxopts::RQUOTE + " option");
        if ((bn_conf.scoring_f != "aic") && (bn_conf.scoring_f != "mdl") && (bn_conf.scoring_f != "bdeu")) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "score" + cxxopts::RQUOTE + " option");
        if (bn_conf.alpha < 0.0) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "alpha" + cxxopts::RQUOTE + " option");

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }


    bool b = false;
    std::tie(b, bn_conf.n, bn_conf.m) = read_csv(io_conf.csv_name, bn_conf.D);

    if (b == false) {
        Log.error() << "could not read input data" << std::endl;
        return -1;
    }

    static_assert(SABNA_MAX_INSTANCE > 4, "SABNA_MAX_INSTANCE must be > 4");

    if (bn_conf.n > 64 * SABNA_MAX_INSTANCE) {
        Log.error() << "too large instance, increase SABNA_MAX_INSTANCE and recompile" << std::endl;
        return -1;
    }

    Log.info() << "input: " << io_conf.csv_name << std::endl;
    Log.info() << "variables: " << bn_conf.n << std::endl;
    Log.info() << "instances: " << bn_conf.m << std::endl;
    Log.info() << "function: " << bn_conf.scoring_f << std::endl;
    Log.info() << "limit: " << bn_conf.pa_size << std::endl;
    Log.info() << "priors: " << io_conf.priors_name << std::endl;
    Log.info() << "output: " << io_conf.net_name << std::endl;

    Log.info() << "searching model" << std::endl;
    auto start = std::chrono::system_clock::now();

    std::pair<bool, std::string> res;

    if (bn_conf.n > 256) res = dispatch<SABNA_MAX_INSTANCE>(io_conf, bn_conf, hc_conf);
    else if (bn_conf.n > 128) res = dispatch<4>(io_conf, bn_conf, hc_conf);
    else if (bn_conf.n > 64) res = dispatch<2>(io_conf, bn_conf, hc_conf);
    else res = dispatch<1>(io_conf, bn_conf, hc_conf);

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    Log.info() << "searching done in " << jaz::log::second_to_time(diff.count()) << std::endl;

    return 0;
} // main
