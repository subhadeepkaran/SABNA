/***
 *  $Id$
 **
 *  File: sabna-pl-mle.cpp
 *  Created: Oct 03, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <iostream>
#include <string>
#include <vector>

#include <cxxopts/cxxopts.hpp>

#include <BVCounter.hpp>
#include <bit_util.hpp>

#include "config.hpp"
#include "csv.hpp"
#include "graph_util.hpp"
#include "log.hpp"
#include "model_fit.hpp"

#include "config.h"


template <int N>
std::pair<bool, std::string> learn_write(int n, int m, const std::vector<signed char>& D, const std::string& sif_name, const std::string& bif_name) {
    BVCounter<N> bvc = create_BVCounter<N>(n, m, std::begin(D));

    network_struct<N> G;
    if (!read_sif(sif_name, n, G)) { return {false, "could not read input network"}; }

    network_fitted<N> fit;
    auto res = mle<N>(bvc, G, fit);

    if (!res.first) return {false, res.second};
    if (!res.second.empty()) Log.warn() << res.second << std::endl;

    if (!write_bif(fit, bif_name)) { return {false, "could not write output network"}; }

    return {true, ""};
} // learn_write


int main (int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-pl-mle");

    std::string csv_name;
    std::string sif_name;
    std::string bif_name;
    std::string type;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("csv-file", "input data file, column-wise without header, white space separated", cxxopts::value<std::string>(csv_name))
            ("sif-file", "input network file, sif structure", cxxopts::value<std::string>(sif_name))
            ("bif-file", "output network file, bif full model", cxxopts::value<std::string>(bif_name))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("csv-file") && opt_res.count("sif-file") && opt_res.count("bif-file")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }

    std::vector<signed char> D;

    bool b = false;
    int n = -1;
    int m = -1;

    std::tie(b, n, m) = read_csv(csv_name, D);

    if (b == false) {
        Log.error() << "could not read input" << std::endl;
        return -1;
    }

    std::pair<bool, std::string> res = { false, "default" };

    Log.info() << "learning parameters" << std::endl;

    static_assert(SABNA_MAX_INSTANCE > 4, "SABNA_MAX_INSTANCE must be > 4");

    if (n > 64 * SABNA_MAX_INSTANCE) {
        Log.error() << "too large instance, increase SABNA_MAX_INSTANCE and recompile" << std::endl;
        return -1;
    }

    if (n <= 64) res = learn_write<1>(n, m, D, sif_name, bif_name);
    else if (n <= 128) res = learn_write<2>(n, m, D, sif_name, bif_name);
    else if (n <= 256) res = learn_write<4>(n, m, D, sif_name, bif_name);
    else res = learn_write<SABNA_MAX_INSTANCE>(n, m, D, sif_name, bif_name);

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    } else Log.info() << "model ready!" << std::endl;

    return 0;
} // main
