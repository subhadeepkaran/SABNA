/***
 *  $Id$
 **
 *  File: sabn-order2net.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *          Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017-2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <iostream>
#include <numeric>

#include <cxxopts/cxxopts.hpp>

#include <BVCounter.hpp>
#include <bit_util.hpp>

#include "MPSList.hpp"
#include "config.hpp"
#include "csv.hpp"
#include "graph_util.hpp"
#include "log.hpp"
#include "model_fit.hpp"

#include "config.h"



template <int N>
std::pair<bool, std::string> load_mps_build_network(BVCounter<N>& bvc, const std::string& mps_name, const std::vector<int>& order, network_struct<N>& G) {
    using set_type = uint_type<N>;

    int n = bvc.n();
    G.resize(n);

    set_type pa = set_empty<set_type>();

    std::ifstream f(mps_name);
    if (!f) return {false, "could not read mps"};

    MPSList<N> mps_list;
    auto res = mps_list.read(n, mps_name);

    if (!res.first) return res;

    for (auto& xi : order) {
        G[xi].range = bvc.r(xi);
        G[xi].pa = mps_list.find(xi, pa).pa;
        pa = set_add(pa, xi);
    }

    return {true, ""};
} // load_mps_build_network


template <int N>
std::pair<bool, std::string> load_build_write(int n, int m, const std::vector<signed char>& D, const std::string& mps_name, const std::vector<int>& order, const std::string& type, const std::string& out) {
    BVCounter<N> bvc = create_BVCounter<N>(n, m, std::begin(D));

    network_struct<N> G;

    auto res = load_mps_build_network(bvc, mps_name, order, G);
    if (!res.first) return res;

    if ((type == "bif") || (type == "net")) {
        Log.info() << "estimating parameters via MLE" << std::endl;
        network_fitted<N> net;
        res = mle(bvc, G, net);
        if (!res.first) return {false, res.second};
        if (!res.second.empty()) Log.warn() << res.second << std::endl;
        if (type == "bif") {
            if (!write_bif(net, out + ".bif")) return {false, "unable to create output file"};
        }
        else {
            if (!write_net(net, out + ".net")) return {false, "unable to create output file"};
        }

    } else {
        if (!write_sif(G, out + ".sif")) return {false, "unable to create output file"};
    }

    return {true, ""};
} // load_build_write


int main (int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-order2net");

    std::string csv_name;
    std::string mps_name;
    std::string ord_name;
    std::string net_name;
    std::string type;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("csv-file", "input data file, column-wise without header, white space separated", cxxopts::value<std::string>(csv_name))
            ("mps-file", "input mps file", cxxopts::value<std::string>(mps_name))
            ("ord-file", "input order file", cxxopts::value<std::string>(ord_name))
            ("net-name", "output network name", cxxopts::value<std::string>(net_name))
            ("format", "output network format [bif|sif|net]", cxxopts::value<std::string>(type)->default_value("bif"))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("csv-file") && opt_res.count("mps-file") && opt_res.count("ord-file") && opt_res.count("net-name")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if ((type != "bif") && (type != "sif") && (type != "net")) throw cxxopts::OptionException("Incorrect format option");

        if (argc != 1) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }


    std::vector<int> order;

    std::ifstream f(ord_name);
    std::istream_iterator<int> ii(f), end;
    std::copy(ii, end, std::back_inserter(order));
    f.close();

    std::vector<signed char> D;

    bool b = false;
    int n = -1;
    int m = -1;

    std::tie(b, n, m) = read_csv(csv_name, D);

    if (b == false) {
        Log.error() << "could not read input" << std::endl;
        return -1;
    }

    if (n != order.size()) {
        Log.error() << "incorrect ordering" << std::endl;
        return -1;
    }

    std::pair<bool, std::string> res = { false, "default" };

    Log.info() << "creating network" << std::endl;

    static_assert(SABNA_MAX_INSTANCE > 4, "SABNA_MAX_INSTANCE must be > 4");

    if (n > 64 * SABNA_MAX_INSTANCE) {
        Log.error() << "too large instance, increase SABNA_MAX_INSTANCE and recompile" << std::endl;
        return -1;
    }

    if (n <= 64) res = load_build_write<1>(n, m, D, mps_name, order, type, net_name);
    else if (n <= 128) res = load_build_write<2>(n, m, D, mps_name, order, type, net_name);
    else if (n <= 256) res = load_build_write<4>(n, m, D, mps_name, order, type, net_name);
    else res = load_build_write<SABNA_MAX_INSTANCE>(n, m, D, mps_name, order, type, net_name);

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    } else Log.info() << "network ready!" << std::endl;

    return 0;
} // main
