#!/bin/bash

cd SABNAtk/
./clean.sh
cd ..

rm -rf bin/
rm -rf build/
rm -rf examples/
rm -rf w64/
rm -rf sabnatk.so
rm -rf autos.*
rm -rf earthquake.*
