# SABNA: Scalable Accelerated Bayesian Network Analytics

**Authors:**
Subhadeep Karan <skaran@buffalo.edu>,
Jaroslaw Zola <jaroslaw.zola@hush.com>

**Contributors:**
John Demetros <johndeme@buffalo.edu>,
Matthew Eichhorn <maeichho@buffalo.edu>,
Blake Hurlburt <blakehur@buffalo.edu>,
Grant Iraci <grantira@buffalo.edu>


## About

SABNA is an open source software suite of efficient algorithms for exact (i.e., globally optimal) Bayesian networks learning. The main idea behind the toolkit is to combine various data optimization techniques and advanced parallel algorithms to achieve scalable implementations capable of processing data instances with hundreds of variables.


## Quick Install

Run `./build.sh` in the project's root directory. The script will create `./bin/` folder containing all SABNA tools.


## Install

Follow these steps:

1. Make sure you have a recent C++ compiler with support for C++14 and SIMD vectorization.
2. Make sure that you have recent Intel TBB deployed in the system.
3. Make sure you have `cmake` 2.8 or newer.
4. Enter SABNA root directory.
5. Optional: edit `config.h` to adjust compilation.
6. Invoke `build.sh` (see examples below).
7. Enjoy!

The package is implemented in C++14 with Intel TBB and it can be compiled using any standard-conforming compiler. Support for SIMD SSE is highly recommended for improved performance (but is not strictly required). We tested extensively `g++` >= 7.3.0, `clang++` >= 4.0.1 and Intel `icpc` >= 18.0.2. If you are familiar with the `cmake` toolchain, you can use standard `cmake` options to customize the installation process, either by editing `build.sh` or setting environment variables.

Examples:

* Installing in custom location: `./build.sh /path/to/install`
* Compiling with Intel compiler: `CXX=icpc ./build.sh`
* Compiling with Intel TBB in non-standard location: `TBB_ROOT=/path/to/intel/tbb ./build.sh`
* Compiling for debugging: edit `build.sh` and add `-DCMAKE_BUILD_TYPE=Debug` to the `cmake` call


## Using

SABNA provides a set of standalone tools, each implementing different functionality. All tools come with an intuitive command line interface. Use `--help` to get a short description of the available CLI options for each tool.

* `sabna-exsl-mpsbuild` precomputes maximal parent sets (MPS) using selected scoring function (currently available: AIC, MDL or BDeu) for any categorical data provided in a csv file.
* `sabna-exsl-bfs-ope` finds optimal network structure for a given MPS using the standard BFS with our _Optimal Path Extension_ (OPE) technique. This is currently the most efficient structure search method.
* `sabna-order2net` converts input ordering into the corresponding DAG written in bif or sif format.

### Example

The example below shows how to learn an exact BN under the MDL score using BFS with OPE (see `test.sh`). We use an example binary dataset `autos.csv` with 26 variables and 159 observations (available in `data/`).

1. First, enumerate MPS: `./sabna-exsl-mpsbuild --csv-file autos.csv --mps-file autos.mps --score mdl`
2. Use the resulting MPS to find an optimal ordering: `./sabna-exsl-bfs-ope -n 26 --mps-file autos.mps --ord-file autos.ord`
3. Convert the resulting ordering into the corresponding structure in BIF format: `./sabna-order2net --csv-file autos.csv --mps-file autos.mps --ord-file autos.ord --net-name autos --format bif`

#### End-to-end example

The example below shows how to learn exact BN given some categorical data (with variables and their states represented by strings). We use `asia.csv` dataset with 8 variables and 200 observations (available in `data/`).

1. Convert input data into SABNA compatible format: `./csv-prepare -H True asia.csv asia`
2. Run SABNA learning steps on the resulting data:
  * `./sabna-exsl-mpsbuild --csv-file asia/asia.sabna.csv --mps-file asia.sabna.mps`
  * `./sabna-exsl-bfs-ope -n 8 --mps-file asia.sabna.mps --ord-file asia.sabna.ord`
  * `./sabna-order2net --csv-file asia/asia.sabna.csv --mps-file asia.sabna.mps --ord-file asia.sabna.ord --net-name asia.sabna --format net`
3. Convert final network back to annotated format: `./net-format.py --map-variables asia/asia.sabna.variables --map-states asia/asia.sabna.states asia.sabna.net asia.net`


## Priors

SABNA provides a mechanism to specify _hard_ priors, which are specific constraints on network structure. Priors are the simplest way to encode prior knowledge about, e.g., causality or imposibility of certain interactions. In SABNA, priors are provided in a text file using simple but flexible syntax. See [data/priors.txt](data/priors.txt) for explanation.


## License

SABNA is completely open source, and released under the MIT License. Some parts of the code are covered by the Boost Software License.


## References

To cite the SABNA package simply refer to this repository. To cite the optimal path extension technique you should refer to our 2016 IEEE BigData paper.

* S. Karan, M. Eichhorn, B. Hurlburt, G. Iraci, J. Zola, _Fast Counting in Machine Learning Applications_, In Proc. Uncertainty in Artificial Intelligence (UAI), 2018. <https://arxiv.org/abs/1804.04640>.
* S. Karan, J. Zola, _Scalable Exact Parent Sets Identification in Bayesian Networks Learning with Apache Spark_, In Proc. IEEE Int. Conference on High Performance Computing, Data, and Analytics (HiPC), pp. 33-41, 2017. <https://arxiv.org/abs/1705.06390>.
* S. Karan, J. Zola, _Exact Structure Learning of Bayesian Networks by Optimal Path Extension_, In Proc. IEEE Int. Conference on Big Data (IEEE BigData), pp. 48-55, 2016. <https://arxiv.org/abs/1608.02682>.
