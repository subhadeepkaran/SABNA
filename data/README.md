## Example Data

| n   | m     | File           | Categories (min/max)  |
| --- | ----- | -------------- | --------------------- |
| 26  | 159   | autos.csv      | 2/2                   |
| 8   | 200   | asia.csv       | 2/2                   |
