#include <iostream>
#include <BVCounter.hpp>
#include "model_fit.hpp"


int main(int argc, const char* argv[]) {
    std::vector<uint8_t> D{0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0,  // 9 0s, 3 1s
                           2, 0, 2, 1, 1, 2, 1, 2, 1, 2, 0, 0,  // 3 0s, 4 1s, 5 2s
                           1, 1, 1, 0, 0 ,0 ,2, 2, 2, 2, 2, 2,  // 3 0s, 3 1s, 6 1s
                           0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,  // 6 0s, 6 1s
                           1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1,  // 2 0s, 10 1s
                           0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1,  // 4 0s, 8 1s
                           2, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2}; // 1 0s, 8 1s, 3 2s

    network_struct<1> net;

    net.resize(7);

    net[0].range = 2;

    net[1].range = 3;
    net[1].pa = set_add(net[1].pa, 0);

    net[2].range = 3;
    net[2].pa = set_add(net[2].pa, 0);

    net[3].range = 2;
    net[3].pa = set_add(net[3].pa, 6);
    net[3].pa = set_add(net[3].pa, 1);

    net[4].range = 2;
    net[4].pa = set_add(net[4].pa, 1);
    net[4].pa = set_add(net[4].pa, 5);

    net[5].range = 2;
    net[5].pa = set_add(net[5].pa, 2);
    net[5].pa = set_add(net[5].pa, 0);
    net[5].pa = set_add(net[5].pa, 1);

    net[6].range = 3;
    net[6].pa = set_add(net[6].pa, 0);

    BVCounter<1> bvc = create_BVCounter<1>(7, 12, std::begin(D));
    auto fit = mle(net, bvc);

    std::string out = "bench";

    write_sif(net, out);
    write_bif(fit, out);

    return 0;
} // main
