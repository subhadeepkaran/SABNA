#!/bin/bash

N=26
IN=data/autos.csv
OUT=autos
PRIOR=data/autos.priors

if [ -n "$PRIOR" ]; then
  bin/sabna-exsl-mpsbuild --csv-file $IN --mps-file $OUT.mps --priors-file $PRIOR --cqe bvc
else
  bin/sabna-exsl-mpsbuild --csv-file $IN --mps-file $OUT.mps --cqe bvc
fi

bin/sabna-exsl-bfs-ope -n $N --mps-file $OUT.mps --ord-file $OUT.ord

bin/sabna-order2net --csv-file $IN --mps-file $OUT.mps --ord-file $OUT.ord --net-name $OUT --format bif
bin/sabna-order2net --csv-file $IN --mps-file $OUT.mps --ord-file $OUT.ord --net-name $OUT --format sif
bin/sabna-order2net --csv-file $IN --mps-file $OUT.mps --ord-file $OUT.ord --net-name $OUT --format net
