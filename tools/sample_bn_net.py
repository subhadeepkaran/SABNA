import copy
import random
import sys
from bayesian_network import BN
from net_reader import NET_FORMAT
from bayesian_network import BN
import pandas as pd

def sample_core(bn_obj, xi, pa_state):
    part_key = ''
    if len(pa_state) != 0:
        part_key = '|' + '|'.join(pa_state)
    row = []
    cum_row = []
    temp = 0
    for state in bn_obj.xi_states[xi]:
        temp_key = part_key + '|' + state + '|'
        row.append(bn_obj.cpt[xi][temp_key])
        pot = bn_obj.cpt[xi][temp_key]
        temp += float(pot)
        cum_row.append(temp)
    rand_val = random.random()
    state_xi = None
    for idx in range(len(cum_row)):
        state_xi = bn_obj.xi_states[xi][idx]
        if rand_val < cum_row[idx]:
            break
     
    return state_xi

def gen_samples(bn_obj, m):
    topo_order = bn_obj.topological_order()

    states_blank = {}
    rows = {}
    for xi in topo_order:
        rows[xi] = []
        states_blank[xi] = "" 

    for _ in range(m):
        states = copy.deepcopy(states_blank)
        for xi in topo_order:
            pa = bn_obj.xi_pa[xi]
            pa_state = []
            for xj in pa:
                pa_state.append(states[xj])
            state_xi = sample_core(bn_obj, xi, pa_state) 
            states[xi] = state_xi
            rows[xi].append(state_xi)       

    return rows 

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print('usage: {} input_net m out_csv'.format(sys.argv[0]))
        sys.exit(-1)

    in_net = sys.argv[1]
    m = int(sys.argv[2])
    oname = sys.argv[3]

    net_obj = NET_FORMAT()
    net_obj.read_net(in_net)
    bn_obj = copy.deepcopy(net_obj.bn_obj)

    rows = gen_samples(bn_obj, m)
    rows = pd.DataFrame(rows)
    rows.to_csv(oname, index=False)
