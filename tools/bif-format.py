#!/usr/bin/env python

__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2018-2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import argparse
import copy
import os
import sys

from bayesian_network import BN
from combination_gen import CombinationGen


def extant_file(fname):
    if not os.path.isfile(fname): raise argparse.ArgumentTypeError("file {0} not found".format(fname))
    return fname


class BIF_FORMAT:
    bn_obj = None

    def initialize_dict(self):
        self.bn_obj = BN()

    def __init__(self):
        self.initialize_dict()

    def __str__(self):
        return self.bn_obj.__str__()

    def __repr__(self):
        return self.bn_obj.__str__()

    def set_bn_obj(self, bn_obj):
        self.bn_obj = copy.deepcopy(bn_obj)

    def get_bn_obj(self):
        return copy.deepcopy(self.bn_obj)

    def read_bif(self, in_name):
        with open(in_name, 'r') as f:
            cur_xi = None
            prob_flag = False
            for l in f:
                l = l.replace('\n', '').replace('\r', '').replace(';', '')
                if 'network ' in l:
                    continue
                elif 'variable ' in l:
                    cur_xi = l.split(' ')[1]
                    self.bn_obj.add_xi(cur_xi)
                elif 'type discrete [' in l:
                    l = [x for x in l.split(' ') if len(x) > 0]
                    self.bn_obj.set_xi_states(cur_xi, [x.replace(',', '') for x in l[6:-1]])
                elif 'probability (' in l:
                    l = [x for x in l.split(' ') if len(x) > 0]
                    cur_xi = l[2]
                    if l[3] == '|':
                        self.bn_obj.set_xi_pa(cur_xi, [x.replace(',', '') for x in l[4:-2]])
                    prob_flag = True
                elif prob_flag:
                    if l == '}':
                        prob_flag = False
                    else:
                        l = [x for x in l.split(' ') if len(x) > 0]
                        states = self.bn_obj.get_xi_states(cur_xi)
                        pa_config = []
                        idx = 1
                        xi_pa = self.bn_obj.get_xi_pa(cur_xi)
                        if len(xi_pa) > 0:
                            pa_config = [x.replace(',', '') for x in l[1: 1 + len(xi_pa)]]
                            idx = 2 + len(xi_pa)

                        prob = [x.replace(',', '') for x in l[idx: idx + len(states)]]

                        for state, prob in zip(states, prob):
                            row = '|' + '|'.join(pa_config + [state]) + '|'
                            self.bn_obj.set_cpt_row(cur_xi, row, prob)

    def sort_pa(self):
        self.bn_obj.sort_pa()

    def sort_states(self):
        self.bn_obj.sort_states()

    def write_bif(self, out_name, dname):
        no_spaces = 2
        spaces = ' ' * no_spaces
        res = ''
        res += 'network ' + dname + ' {' + '\n'
        res += '}' + '\n'

        for xi in self.bn_obj.get_all_xi():
            states = self.bn_obj.get_xi_states(xi)
            res += 'variable ' + xi + ' {' + '\n'
            res += spaces + 'type discrete [ ' + str(len(states)) + ' ] { ' + ', '.join(states) + ' }' + '\n'
            res += '}' + '\n'

        for xi, cpt in self.bn_obj.cpt.items():
            xi_states = self.bn_obj.get_xi_states(xi)
            xi_arity = len(xi_states)
            all_xi = self.bn_obj.get_xi_pa(xi)
            all_xi.append(xi)

            pa_info = ''
            if len(all_xi) > 1:
                pa_info = ' | ' + ', '.join(all_xi[:-1]) + ' '
            pa_info += ') '
            res += 'probability ( ' + xi + ' ' + pa_info + '{' + '\n'

            state_range = []
            for xj in all_xi:
                state_range.append(len(self.bn_obj.get_xi_states(xj)) - 1)

            val_row = list()
            comb_gen = CombinationGen(state_range)
            for state_config_idx in comb_gen.generator():
                row = []
                for xj, idx in zip(all_xi, state_config_idx):
                    row.append(self.bn_obj.get_xi_states(xj)[idx])
                key = '|' + '|'.join(row) + '|'
                val = self.bn_obj.get_cpt_row_val(xi, key)

                val_row.append(val)
                if len(val_row) ==  xi_arity:
                    row = ', '.join(row[:-1])
                    val_row = ', '.join(val_row)
                    row_info = 'table '
                    if len(all_xi) > 1:
                        row_info = '( ' + row + ' ) '
                    res += spaces + row_info + val_row + ';' + '\n'
                    val_row = []

            res += '}' + '\n'

        with open(out_name, 'w') as f:
            f.write(res)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("input_bif", help = "input bif file", type = extant_file)
    parser.add_argument("output_bif", help = "output bif file")
    parser.add_argument("network_name", help = "name of the dataset")
    parser.add_argument("--ss", help = "sort states", nargs = '?', type = bool, const = False, default = False)
    parser.add_argument("--spa", help = "sort parents", nargs = '?', type = bool, const = False, default = False)
    parser.add_argument("--map-variables", help = "mapping from sabna.csv.variables", nargs = '?', const = None, default = None, type = extant_file)
    parser.add_argument("--map-states", help = "mapping from sabna.csv.states", nargs = '?', const = None, default = None, type = extant_file)

    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    in_name = args.input_bif
    out_name = args.output_bif
    dname = args.network_name
    sort_states = args.ss
    sort_parents = args.spa
    xi_mapper_in = args.map_variables
    xi_states_mapper_in = args.map_states

    bif_obj = BIF_FORMAT()
    bif_obj.read_bif(in_name)

    if sort_states:
        bif_obj.sort_states()
    if sort_parents:
        bif_obj.sort_pa()

    if xi_mapper_in != None and xi_states_mapper_in != None:
        bif_obj.bn_obj.sabna_mapping(xi_mapper_in, xi_states_mapper_in)
    bif_obj.write_bif(out_name, dname)
