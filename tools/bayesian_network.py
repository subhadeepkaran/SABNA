#!/usr/bin/env python

__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2018-2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import copy
from sabna_mapping_reader import SABNA_MAPPER

class BN:
    xi_states = None
    xi_pa = None
    cpt = None
    adj_list = None

    def __init__(self):
        self.xi_states = dict()
        self.xi_pa = dict()
        self.cpt = dict()
        self.adj_list = dict()

    def __str__(self):
        return self.represent_as_str()

    def __repr__(self):
        return self.represent_as_str()

    def represent_as_str(self):
        if len(self.xi_states) == 0:
            return 'Empty Bayesian Network'
        res = 'Number of variables: ' + str(len(self.xi_states)) + '\n'
        res += 'Average arity: ' + str(self.compute_average_arity()) + '\n'
        res += 'Range of arity: [' + str(self.get_min_max_arity()) + ']\n\n'

        order_xi = self.topological_order()
        res += 'Variables: {}'.format(order_xi) + "\n"
        for xi in order_xi:
            res += 'Var: ' + xi + ' | States: ' + ' '.join(self.xi_states[xi]) + '\n'
            res += 'Parents: ' + '|'.join(self.xi_pa[xi]) + '\n'
            res += 'CPT (Parents(xi) | xi) \n '
            for k in self.cpt[xi].keys():
                res += '\t{} : {}'.format(k, self.cpt[xi][k]) + '\n'
            res += '\n'

        res += 'Adj list: \n'
        self.build_adj_list()
        for xi in order_xi:
            res += xi + ' -- ' + '|'.join(self.adj_list[xi]) + '\n'
        return res

    def has_xi(self, xi):
        if xi in self.xi_states and len(self.xi_states[xi]) > 0:
            return True
        return False

    def add_xi(self, xi):
        self.xi_states[xi] = list()
        self.xi_pa[xi] = list()
        self.cpt[xi] = dict()
        self.adj_list[xi] = dict()

    def get_all_xi(self):
        return list(self.xi_states.keys())[:]

    def set_xi_states(self, xi, states):
        self.xi_states[xi] = states[:]

    def get_xi_states(self, xi):
        return self.xi_states[xi][:]

    def set_xi_pa(self, xi, pa):
        self.xi_pa[xi] = pa[:]

    def get_xi_pa(self, xi):
        return self.xi_pa[xi][:]

    def set_cpt_row(self, xi, row, val):
        self.cpt[xi][row] = val

    def get_cpt_row_val(self, xi, row):
        return self.cpt[xi][row]

    def build_adj_list(self):
        self.adj_list = dict()
        for xi in self.xi_states.keys():
            self.adj_list[xi] = []
        for xi in self.xi_pa.keys():
            for xj in self.xi_pa[xi]:
                self.adj_list[xj].append(xi)
        return copy.deepcopy(self.adj_list)

    def sort_states(self):
        for xi in self.xi_states.keys():
            states = self.xi_states[xi]
            sort_states = states[:]
            sort_states.sort()
            self.xi_states[xi] = sort_states[:]

    def sort_pa(self):
        xi_list = self.xi_states.keys()

        for xi in xi_list:
            org_pa = self.xi_pa[xi]
            new_pa = org_pa[:]
            new_pa.sort()

            new_pos = [new_pa.index(xj) for xj in org_pa]
            new_pos.append(len(org_pa))
            ht = {}
            for state in self.cpt[xi].keys():
                tstate = state
                val = self.cpt[xi][state]
                self.cpt[xi].pop(state, None)
                state_list = [x for x in state.split('|') if len(x) > 0]

                new_state = [None] * len(state_list)
                for i in range(len(state_list)):
                    new_state[new_pos[i]] = state_list[i]

                new_state = '|' + '|'.join(new_state) + '|'
                ht[new_state] = val
            self.xi_pa[xi] = new_pa[:]
            self.cpt[xi] = copy.deepcopy(ht)

    def compute_arity_size(self, xi = None):
        res = list()
        for xi in self.xi_states.keys():
            res.append(len(self.xi_states[xi]))
        return res

    def compute_average_arity(self):
        res = self.compute_arity_size()
        return float(sum(res))/len(res)

    def get_min_max_arity(self):
        res = self.compute_arity_size()
        return min(res), max(res)

    def size_of_cpt_tables(self, xi_list = None):
        if xi_list is None:
            return sum([len(self.cpt[xi]) for xi in self.xi_states.keys()])
        else:
            return len(self.cpt[xi])

    def topological_order(self):
        roots = []
        for xi in self.xi_pa.keys():
            if len(self.xi_pa[xi]) == 0:
                roots.append(xi)

        visited = {}
        for xi in self.xi_pa.keys():
            visited[xi] = False

        order = []
        self.build_adj_list()
        for xi in roots:
            visited[xi] = True
            for xj in self.adj_list[xi]:
                if visited[xj]: continue
                visited[xj] = True
                self.dfs(xj, visited, order)
            order.append(xi)
        order.reverse()

        return order

    def dfs(self, xi, visited, order):
        for xj in self.adj_list[xi]:
            if visited[xj]: continue
            visited[xj] = True
            self.dfs(xj, visited, order)
        order.append(xi)

    def sabna_mapping(self, xi_mapper_in, state_mapper_in):
        sabna_mapper = SABNA_MAPPER(xi_mapper_in, state_mapper_in)

        new_cpt = dict()
        new_xi_states = dict()
        new_xi_pa = dict()
        for oxi in self.xi_states.keys():
            nxi = sabna_mapper.sabna_forward_mapping_xi(oxi)
            new_cpt[nxi] = dict()
            new_xi_states[nxi] = list()
            new_xi_pa[nxi] = list()

        for oxi, states in self.xi_states.items():
            nxi = sabna_mapper.sabna_forward_mapping_xi(oxi)
            for state in states:
                new_xi_states[nxi].append(sabna_mapper.sabna_forward_mapping_xi_state(oxi, state))

        for xi, pa in self.xi_pa.items():
            nxi = sabna_mapper.sabna_forward_mapping_xi(xi)
            for xj in pa:
                new_xi_pa[nxi].append(sabna_mapper.sabna_forward_mapping_xi(xj))

        for xi, cpt in self.cpt.items():
            all_xi = list()
            for xj in self.xi_pa[xi]:
                all_xi.append(xj)
            all_xi.append(xi)

            nxi = sabna_mapper.sabna_forward_mapping_xi(xi)
            for ostates, prob in cpt.items():
                ostates = [x for x in ostates.split('|') if len(x) > 0]

                nstates = list()
                for xj, state in zip(all_xi, ostates):
                    nstates.append(sabna_mapper.sabna_forward_mapping_xi_state(xj, state))
                nstates = '|' + '|'.join(nstates) + '|'
                new_cpt[nxi][nstates] = prob

        self.xi_states = copy.deepcopy(new_xi_states)
        self.xi_pa = copy.deepcopy(new_xi_pa)
        self.cpt = copy.deepcopy(new_cpt)
