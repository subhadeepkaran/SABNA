#!/usr/bin/env python

__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2017-2018 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import argparse
import csv
import os
import sys


def binarize_data(mapping_state_xi, D):
    for xi in range(len(D)):
        if len(mapping_state_xi[xi]) <= 2: continue

        mean_val = len(mapping_state_xi[xi])/2
        new_mapping = {}
        temp_mapping = {}
        temp0 = ""
        temp1 = ""
        for r in mapping_state_xi[xi]:
            if mapping_state_xi[xi][r] <= mean_val:
                temp_mapping[r] = 0
                temp0 += "(" + r + ")"
            else:
                temp_mapping[r] = 1
                temp1 += "(" + r + ")"

        Dxi = []
        for val in D[xi]:
            temp = 1
            if val <= mean_val: temp = 0
            Dxi.append(temp)
        D[xi] = Dxi

        new_mapping[temp0] = 0
        new_mapping[temp1] = 1
        mapping_state_xi[xi] = new_mapping

    return mapping_state_xi, D

def create_directory(dir_name): os.makedirs(dir_name)

def write_files(opt, D, mapping_state_xi, header_list):
    dir_name = opt['dir_name']
    dname = opt['dname']
    oname = dir_name + "/" + dname + ".sabna.csv"

    with open(oname, 'w') as f:
        for d in D:
            d = " ".join([str(x) for x in d]) + "\n"
            f.write(d)

    oname = dir_name + "/" + dname + ".sabna.summary"

    avg_arity = 0.0
    min_arity = float('INF')
    max_arity = -1

    for k in mapping_state_xi:
        avg_arity += len(mapping_state_xi[k])
        min_arity = min(min_arity, len(mapping_state_xi[k]))
        max_arity = max(max_arity, len(mapping_state_xi[k]))

    avg_arity /= len(D)

    with open(oname, 'w') as f:
        temp = "name: {}".format(dname)
        print(temp)
        f.write(temp + "\n")
        temp = "variables: {}".format(len(D))
        print(temp)
        f.write(temp + "\n")
        temp = "observations: {}".format(len(D[0]))
        print(temp)
        f.write(temp + "\n")
        temp = "average arity: {:0.2f}".format(avg_arity)
        print(temp)
        f.write(temp + "\n")
        temp = "arity range: [{},{}]".format(min_arity, max_arity)
        print(temp)
        f.write(temp + "\n")
        temp = "binarized: {}\n".format(opt['binarize_input'])
        print(temp)


    if min_arity == 1:
        print("warning: min arity is 1, some variables are uninformative!")


    oname = dir_name + "/" + dname + ".sabna.variables"

    with open(oname, 'w') as f:
        for xi in range(len(D)):
            f.write("{}\t{}\n".format(header_list[xi], xi))

    oname = dir_name + "/" + dname + ".sabna.states"

    with open(oname, 'w') as f:
        for xi in range(len(D)):
            for r in mapping_state_xi[xi]: f.write("{}\t{}\t{}\n".format(header_list[xi], r, mapping_state_xi[xi][r]))
    return

def extant_file(fname):
    if not os.path.isfile(fname): raise argparse.ArgumentTypeError("file {0} not found".format(fname))
    return fname

def extant_directory(dname):
    if os.path.exists(dname): raise argparse.ArgumentTypeError("{0} directory already exists".format(dname))
    return dname

def read_file(opt):
    D = []
    m_list = []
    n = 0
    m = 0

    with open(opt['fname']) as csvfile:
        delim = opt['delimiter']
        if not opt['delimiter']:
            dialect = csv.Sniffer().sniff(csvfile.read(100000))
            csvfile.seek(0)
            delim = dialect.delimiter

        for l in csvfile:
            l = l.replace("\n", "").replace("\r", "").split(delim)
            m_list.append(len(l))
            D += l
            n += 1

    if not len(set(m_list)) == 1: return (False, "incorrect row size in {}".format(opt['fname']), n, m, D) 

    m = m_list[0]

    return (True, "", n, m, D)

def transform_D_to_2D_and_transpose(n, m, D, opt):
    perform_transposition = opt['perform_transposition']

    new_D = None
    if perform_transposition: new_D = [[] for _ in range(m)]
    else: new_D = [[] for _ in range(n)]

    D.reverse()
    for i in range(n):
        for j in range(m):
            if not perform_transposition: new_D[i].append(D[j  + i * m])
            else: new_D[j].append(D[j + i * m])

    if perform_transposition: return (m, n, new_D)
    return (n, m, new_D)

def extract_headers(D):
    header_list = []
    for Dxi in D: header_list.append(Dxi.pop())
    return header_list, D, len(D[0])

def cast_str_to_bool(val): return val == 'True'

def handle_missing_values(D):
    idx = set()
    for xi in range(n):
        for i in range(len(D[0])):
            if D[xi][i] == "": idx.add(i)

    idx = list(idx)
    idx.sort(reverse = True)

    if len(idx) == len(D[0]): return (None, "terminating, all rows consist of missing values")

    for i in idx:
        for xi in range(n): del(D[xi][i])

    return (D, "")

def make_state_mapping(D):
    n = len(D)
    mapping_state_xi = {}

    for xi in range(n):
        mapping_state_xi[xi] = {}
        counter = 0

        temp = list(set(D[xi]))
        temp.sort()
        for x in temp: 
            mapping_state_xi[xi][x] = counter
            counter += 1

        Dxi = []
        for val in D[xi]: 
            Dxi.append(mapping_state_xi[xi][val])
        Dxi.reverse()
        D[xi] = Dxi

    # sort for compatibility with bnlearn
    for i in iter(mapping_state_xi):
        pos = 0
        l = list(mapping_state_xi[i].keys())
        l.sort()
        for k in l:
            mapping_state_xi[i][k] = pos
            pos = pos + 1

    return (mapping_state_xi, D)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("input", metavar="", help = "input csv file", type = extant_file)
    parser.add_argument("output", metavar="", help = "output directory name", type = extant_directory)
    parser.add_argument("-H", "--header", metavar="", help = "input file has header", nargs='?', type = str, choices=['False', 'True'], const = 'False', default = 'True')
    parser.add_argument("-s", "--separator", metavar="", help = "separator, guessed by default", nargs='?', const = None, type = str)
    parser.add_argument("-t", "--transpose", metavar="", help = "transpose input",  nargs='?', type = str, choices=['False', 'True'], const = 'False', default = 'True')
    parser.add_argument("-n", "--naignore", metavar="", help = "ignore observations with missing values", nargs='?', type = str, choices=['False', 'True'], const = 'False', default = 'True')
    parser.add_argument("-b", "--binarize", metavar="", help = "convert to binary", nargs='?', type = str, choices=['False', 'True'], const = 'False', default = 'False')

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    opt = {}
    opt['fname'] = args.input
    opt['dname'] = os.path.splitext(os.path.basename(opt['fname']))[0]
    opt['dir_name'] = args.output
    opt['has_header'] = cast_str_to_bool(args.header)
    opt['perform_transposition'] = cast_str_to_bool(args.transpose)
    opt['delimiter'] = args.separator
    opt['binarize_input'] = cast_str_to_bool(args.binarize)
    opt['ignore_missing_value'] = cast_str_to_bool(args.naignore)

    D = []
    n = -1
    m = -1
    header_list = None

    is_successful, msg, n, m, D = read_file(opt)
    if not is_successful:
        print(msg)
        sys.exit(-1)

    n, m, D = transform_D_to_2D_and_transpose(n, m, D, opt)

    header_list = range(n -1, -1, -1)
    if opt['has_header']: header_list, D, m = extract_headers(D)
    if opt['ignore_missing_value']: D, msg = handle_missing_values(D)

    if not D:
        print(msg)
        sys.exit(-1)

    mapping_state_xi, D = make_state_mapping(D)
    if opt['binarize_input']: mapping_state_xi, D = binarize_data(mapping_state_xi, D)

    create_directory(opt['dir_name'])
    write_files(opt, D, mapping_state_xi, header_list)
