#!/usr/bin/env python

__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2018-2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import sys


class SABNA_MAPPER:
    states_map = None
    oxi_nxi = None
    nxi_oxi = None

    def __init__(self, xi_mapping_in, state_mapping_in):
        self.states_map = dict()
        self.oxi_nxi = dict()
        self.nxi_oxi = dict()
        self.read_xi_mapping(xi_mapping_in)
        self.read_state_mapping(state_mapping_in)

    def __str__(self):
        return self.represent_as_str()

    def __repr__(self):
        return self.represent_as_str()

    def represent_as_str(self):
        res = ''
        res += 'Forward Variable Mapping' + '\n'
        for oxi, nxi in self.oxi_nxi.items():
            res += oxi + ' -> ' + nxi + '\n'

        res += '\n' + 'Reverse Variable Mapping' + '\n'
        for nxi, oxi in self.nxi_oxi.items():
            res += nxi + ' -> ' + oxi + '\n'

        res += '\n' + 'Forward State Mapping'
        for oxi, states in self.states_map.items():
            res += oxi + " | states: " + str(states) + '\n'

        return res

    def read_xi_mapping(self, fname):
        with open(fname, 'r') as f:
            for l in f:
                xi_id, xi = l.replace('\n', '').replace('\r', '').split('\t')
                self.oxi_nxi[xi] = xi_id
                self.states_map[xi] = dict()
                self.nxi_oxi[xi_id] = xi

    def read_state_mapping(self, fname):
        with open(fname, 'r') as f:
            for l in f:
                xi, state, state_imap = l.replace('\n', '').replace('\r', '').split('\t')
                self.states_map[self.sabna_reverse_mapping_xi(xi)][state_imap] = state

    def sabna_reverse_mapping_xi(self, xi):
        return self.nxi_oxi[xi]

    def sabna_forward_mapping_xi(self, xi):
        return self.oxi_nxi[xi]

    def sabna_forward_mapping_xi_state(self, xi, state):
        return self.states_map[xi][state]


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: {} xi_mapping_file state_mapping_file'.format(sys.argv[0]))
        sys.exit(-1)

    xi_mapper_in = sys.argv[1]
    state_mapper_in = sys.argv[2]

    sabna_mapper = SABNA_MAPPER(xi_mapper_in, state_mapper_in)
    print(sabna_mapper)
