`bayesian_network.py`: Representation of any Bayesian Network with categorial data.

* Converison of format using Python:
... Example: `NET` to `BIF`:

```
net_obj = NET_FORMAT()
net_obj.read_net(input_net)

bif_obj = BIF_FORMAT()
bif_obj.set_bn_obj(net_obj.get_bn_obj())
bif_obj.write_bif(output_bif)
```

* Mapping back from SABNA index variable to the original variables (names and states):

```net-format.py input_net output_net --map-variables dataset_name.sabna.variables --map-states dataset_name.sabna.states```
