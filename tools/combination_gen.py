#!/usr/bin/env python

__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2018-2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"


class CombinationGen:
    cur = None
    ref = None

    def __init__(self, ref):
        self.ref = ref
        self.reset()

    def custom_start(self, start):
        self.cur = start

    def reset(self):
        self.cur = [0 for _ in range(len(self.ref))]
        self.cur[-1] = -1

    def find_index(self):
        for i in range(len(self.ref) - 1, -1, -1):
            if self.cur[i] != self.ref[i]:
                return i
        return None

    def generator(self):
        while self.cur != self.ref:
            idx = self.find_index()
            if idx != None:
                self.cur[idx] += 1
                for i in range(idx + 1, len(self.cur)):
                    self.cur[i] = 0
                yield self.cur
