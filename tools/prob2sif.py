#!/usr/bin/env python

__author__ = "Jaroslaw Zola"
__copyright__ = "Copyright (c) 2018 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Jaroslaw Zola"
__email__ = "jaroslaw.zola@hush.com"
__status__ = "Development"

import argparse
import os
import sys


def extant_file(fname):
    if not os.path.isfile(fname): raise argparse.ArgumentTypeError("file {0} not found".format(fname))
    return fname

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("input", metavar="", help = "input probability file", type = extant_file)
    parser.add_argument("output", metavar="", help = "output network file", type = str)
    parser.add_argument("threshold", metavar="", help = "threshold to return edge", type = float)

    if len(sys.argv) != 4:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    input = args.input
    output = args.output
    t = args.threshold

    with open(input) as f:
        L = f.readlines()

    L = [x.strip() for x in L]
    L = [x.split() for x in L]

    X = set()
    Z = set()

    for x in L:
        X.add(x[0])
        X.add(x[1])

    E = [x for x in L if float(x[2]) > t and float(x[3]) > 0.5]

    for x in E:
        Z.add(x[0])
        Z.add(x[1])

    with open(output, 'w') as f:
        for x in E:
            f.write(x[0] + ' -> ' + x[1] + '\n')

        for x in X:
            if not x in Z:
                f.write(x + ' -> ' + x + '\n')
