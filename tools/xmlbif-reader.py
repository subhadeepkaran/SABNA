#!/usr/bin/env python

__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2018-2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import argparse
import copy
import sys
import os
import xml.etree.ElementTree as ET

from bayesian_network import BN
from combination_get import CombinationGen


def extant_file(fname):
    if not os.path.isfile(fname): raise argparse.ArgumentTypeError("file {0} not found".format(fname))
    return fname

class XMLBIF_FORMAT:
    bn_obj = None

    def __init__(self):
        self.bn_obj = BN()

    def read_xmlbif(self, in_name):
        tree = ET.parse(in_name)
        root = tree.getroot()

        for x in root.findall('NETWORK'):
            for y in x.iter('VARIABLE'):
                cur_var = None
                for z in y.iter('NAME'):
                    cur_var = z.text
                    self.bn_obj.xi_states[cur_var] = []
                for z in y.iter('OUTCOME'):
                    self.bn_obj.xi_states[cur_var].append(z.text)

            for y in x.iter('DEFINITION'):
                cur_var = None
                for z in y.iter('FOR'):
                    cur_var = z.text
                    self.bn_obj.xi_pa[cur_var] = []
                xi_list = []
                states = []
                for z in y.iter('GIVEN'):
                    self.bn_obj.xi_pa[cur_var].append(z.text)
                    states.append(len(self.bn_obj.xi_states[cur_var]) - 1)
                    xi_list.append(z.text)
                xi_list.append(cur_var)
                states.append(len(self.bn_obj.xi_states[cur_var]) - 1)

                for z in y.iter('TABLE'):
                    self.bn_obj.cpt[cur_var] = {}
                    val_list = [x for x in z.text.split(' ') if len(x) > 0]
                    comb_obj = CombinationGen(states)
                    idx = 0
                    for x in comb_obj.generator():
                        temp_key = '|'
                        for i, xi in zip(x, xi_list):
                             temp_key += self.bn_obj.xi_states[xi][i] + '|'
                        self.bn_obj.cpt[cur_var][temp_key] = val_list[idx]
                        idx +=1

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("input_xmlbif", help = "input xmlbif file", type = extant_file)
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    xmlbif_name = args.input_xmlbif

    xmlbif_obj = XMLBIF_FORMAT()
    xmlbif_obj.read_xmlbif(xmlbif_name)
