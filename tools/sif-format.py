#!/usr/bin/env python

__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2018-2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import argparse
import os
import sys


def extant_file(fname):
    if not os.path.isfile(fname): raise argparse.ArgumentTypeError("file {0} not found".format(fname))
    return fname

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("input_sif", help = "input sif file", type = extant_file)
    parser.add_argument("output_sif", help = "output sif file")
    parser.add_argument("--map-variables", help = "mapping from sabna.csv.variables", nargs = '?', const = None, default = None, type = extant_file)

    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    in_name = args.input_sif
    out_name = args.output_sif
    xi_mapper_in = args.map_variables

    with open(in_name) as f:
        L = f.readlines()

    if xi_mapper_in != None:
        L = [x.strip() for x in L]
        L = [x.split(' -> ') for x in L]

        D = {}

        with open(xi_mapper_in) as f:
            for line in f:
                (value, key) = line.strip().split('\t')
                D[key] = value

        with open(out_name, 'w') as f:
            for x in L:
                f.write(D[x[0]] + ' -> ' + D[x[1]] + '\n')
    else:
        with open(out_name, 'w') as f:
            for x in L: f.write(x)
