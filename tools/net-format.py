#!/usr/bin/env python

__author__ = "Subhadeep Karan"
__copyright__ = "Copyright (c) 2018-2019 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Subhadeep Karan"
__email__ = "skaran@buffalo.edu"
__status__ = "Development"

import argparse
import copy
import os
import sys

from bayesian_network import BN
from combination_gen import CombinationGen


def extant_file(fname):
    if not os.path.isfile(fname): raise argparse.ArgumentTypeError("file {0} not found".format(fname))
    return fname


class NET_FORMAT:
    bn_obj = None

    def initialize_dict(self):
        self.bn_obj = BN()

    def __init__(self):
        self.initialize_dict()

    def set_bn_obj(self, bn_obj):
        self.bn_obj = copy.deepcopy(bn_obj)

    def get_bn_obj(self):
        return copy.deepcopy(self.bn_obj)

    def read_net(self, in_name):
        temp_cpt = {}
        with open(in_name, 'r') as f:
            cur_var = None
            pa = []

            for l in f:
                l = l.replace('\n', '')

                if 'node ' in l:
                    cur_var = l.replace("node ", "").strip()
                    self.bn_obj.add_xi(cur_var)
                elif 'states = ' in l:
                    states = l.replace('states = ', "").replace("(", "").replace(");", "")
                    states = [x.replace("\"", "") for x in states.split(" ") if len(x) > 0]
                    self.bn_obj.set_xi_states(cur_var, states)
                elif 'potential ' in l:
                    l = l.replace("potential ( ", "").replace(" )", "").split(" | ")
                    cur_var = l[0].strip()
                    pa = []
                    if len(l) > 1:
                        pa = [x.strip() for x in l[1].split(" ") if len(x.strip()) > 0]
                    self.bn_obj.set_xi_pa(cur_var, pa)
                elif 'data ' in l:
                    l = l.replace('data = ', '').replace(';', '').strip()
                    states = [0 for _ in range(len(pa) + 1)]
                    pa = self.bn_obj.get_xi_pa(cur_var)
                    pa.append(cur_var)
                    num_states = []
                    for x in pa:
                        num_states.append(len(self.bn_obj.get_xi_states(x)) - 1)

                    new_l = [x for x in self.format_potential_line(l) if not (x == '(' or x == ')')]
                    comb_obj = CombinationGen(num_states)
                    gen = comb_obj.generator()
                    idx = 0
                    temp_cpt[cur_var] = dict()
                    for x in gen:
                        key = ','.join([str(y) for y in x])
                        temp_cpt[cur_var][key] = new_l[idx]
                        idx += 1

        self.map_potentials(temp_cpt)

    def write_net(self, oname):
        with open(oname, 'w') as f:
            temp = "net\n{\n}\n"
            f.write(temp)
            for xi in self.bn_obj.get_all_xi():
                xi_states = self.bn_obj.get_xi_states(xi)
                temp = "node " + xi + "\n{" + "\n"
                f.write(temp)
                temp = "  states = ("
                for state in xi_states:
                    temp += " \"" + state + "\""
                temp += " );\n"
                f.write(temp)
                f.write("}\n")

            for xi in self.bn_obj.get_all_xi():
                xi_states = self.bn_obj.get_xi_states(xi)
                temp = "potential ( " + xi
                pa = self.bn_obj.get_xi_pa(xi)
                if len(pa) > 0 :
                    temp += " | " + ' '.join(pa)
                temp += " )\n"
                f.write(temp)
                f.write('{\n')
                temp = "  data = "
                res = ""
                res = self.gen_states(xi, res)
                temp += res;
                f.write(temp + ";\n")
                f.write('}\n')

    def sort_pa(self):
        self.bn_obj.sort_pa()

    def sort_states(self):
        self.bn_obj.sort_states()

    def map_potentials(self, temp_cpt):
        for xi in self.bn_obj.get_all_xi():
            pa = self.bn_obj.get_xi_pa(xi)
            pa.append(xi)
            for k,v in temp_cpt[xi].items():
                k = [int(x) for x in k.split(',')]
                map_k = []
                temp_key = ''
                for i in range(len(k)):
                    state = self.bn_obj.get_xi_states(pa[i]) [k[i]]
                    temp_key = temp_key + '|' + state
                temp_key = temp_key + '|'
                self.bn_obj.set_cpt_row(xi, temp_key, v)

    def format_potential_line(self, line):
        insert = False
        temp = ''
        res = []
        for x in list(line):
            if len(x) == 0:
                continue
            if x != "(" and x != ")" and x != " " :
                insert = True
            else:
                res.append(temp)
                temp = x
                insert = False
            if insert:
                temp += x
            elif x != "":
                res.append(x)
                temp = ''
        return [x for x in res if len(x) > 0 and x != ' ']

    def get_row_given_pa_state(self, xi, pa_state):
        res = '( '
        nk = '|' + '|'.join(pa_state)
        if len(pa_state) != 0:
            nk += '|'
        for state in self.bn_obj.get_xi_states(xi):
            temp_key = nk + state + '|'
            res += self.bn_obj.get_cpt_row_val(xi, temp_key) + " "
        res += ")"
        return res

    def put_parenthesis(self, l, ri):
        bias = 1
        for i in range(len(ri) - 1, -1, -1):
            bias *= ri[i]
            counter = 1
            while counter * bias <= len(l):
                l[counter * bias - 1] += ")"
                counter += 1

        bias = 1
        for i in range(len(ri) - 1, -1, -1):
            bias *= ri[i]
            counter = 1
            while counter * bias <= len(l):
                l[(counter - 1) * bias ] = "(" +  l[(counter - 1) * bias ]
                counter += 1
        return l

    def gen_states(self, xi, res):
        pa = self.bn_obj.get_xi_pa(xi)
        pa_state = []
        if len(pa) == 0:
            return self.get_row_given_pa_state(xi, pa_state)

        states = [len(self.bn_obj.get_xi_states(xj)) for xj in pa]
        cur_states = [0 for _ in range(len(states))]
        res = 'INVALID'
        if cur_states != states:
            l = []
            comb_obj = CombinationGen([x - 1 for x in states])
            gen = comb_obj.generator()
            for x in gen:
                pa_state = []
                for i in range(len(pa)):
                    pa_state.append(self.bn_obj.get_xi_states(pa[i])[x[i]])
                l.append(self.get_row_given_pa_state(xi, pa_state))

            res = l
            self.put_parenthesis(res, states)
            res = ''.join(res)
        return res


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("input_net", help = "input net file", type = extant_file)
    parser.add_argument("output_net", help = "output net file")
    parser.add_argument("--ss", help = "sort states", nargs = '?', type = bool, const = False, default = False)
    parser.add_argument("--spa", help = "sort parents", nargs = '?', type = bool, const = False, default = False)
    parser.add_argument("--map-variables", help = "mapping from sabna.csv.variables", nargs = '?', const = None, default = None, type = extant_file)
    parser.add_argument("--map-states", help = "mapping from sabna.csv.states", nargs = '?', const = None, default = None, type = extant_file)
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    in_name = args.input_net
    out_name = args.output_net
    sort_states = args.ss
    sort_parents = args.spa
    xi_mapper_in = args.map_variables
    xi_states_mapper_in = args.map_states

    net_obj = NET_FORMAT()
    net_obj.read_net(in_name)

    if sort_states:
        net_obj.sort_states()
    if sort_parents:
        net_obj.sort_pa()

    if xi_mapper_in != None and xi_states_mapper_in != None:
        net_obj.bn_obj.sabna_mapping(xi_mapper_in, xi_states_mapper_in)
    net_obj.write_net(out_name)
