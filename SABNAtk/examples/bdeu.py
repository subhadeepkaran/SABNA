from bitarray import bitarray
from sabnatk.BVCounter import BDeu64

import csv


if __name__ == "__main__":
    dat = "data/alarm.csv"

    n = 0
    m = 0
    D = []

    with open(dat, "rt") as cf:
        rd = csv.reader(cf, delimiter = ' ')
        for row in rd:
            n = n + 1
            m = len(row)
            D = D + map(int, row)

    bdeu = BDeu64()

    bdeu.init(n, m, D)
    bdeu.alpha = 0.1

    xi = bitarray(37, endian = "little")
    xi.setall(0)
    xi[0] = 1;
    xi[7] = 1;
    xi[9] = 1;

    pa = bitarray(37, endian = "little")
    pa.setall(0)
    pa[3] = 1;
    pa[5] = 1;
    pa[8] = 1;
    pa[29] = 1;
    pa[30] = 1;
    pa[31] = 1;

    S = bdeu.score(xi.tobytes(), pa.tobytes())
    print "test 0.1:", S

    bdeu.alpha = 10.0

    S = bdeu.score(xi.tobytes(), pa.tobytes())
    print "test 10:", S
