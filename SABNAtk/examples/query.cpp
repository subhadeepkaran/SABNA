#include <iostream>
#include <BVCounter.hpp>


struct call {
    // called by query engine before processing the stream
    // engine passes ri, i.e. the number of states of Xi,
    // and qi, the number of states parents of Xi MAY assume
    void init(int ri, int qi) { count = 0; }

    // called by query engine after processing of the stream is done
    // engine passes qi, the ACTUAL number of states
    // that parents of Xi assumed
    void finalize(int qi) { }

    void operator()(int Nij) {
        std::cout << "call from CQE with Nij=" << Nij << std::endl;
    } // operator()

    void operator()(int Nijk, int Nij) {
        std::cout << "call from CQE with Nijk=" << Nijk << std::endl;
        count++;
    } // operator()

    // access to internal state (return value is specified by user)
    int score() const { return count; }

    // user specified internal state
    int count = 0;
}; // struct call


int main(int argc, char* argv[]) {
    // 3 variables (say X0, X1, X2), 8 observations
    std::vector<char> D{0, 1, 1, 0, 1, 1, 0, 0, \
                        0, 0, 2, 0, 1, 2, 0, 1, \
                        1, 1, 1, 0, 1, 1, 1, 0 };

    // use one word (64bit) because n < 64
    BVCounter<1> bvc = create_BVCounter<1>(3, 8, std::begin(D));

    using set_type = BVCounter<1>::set_type;

    auto xi = set_empty<set_type>();
    auto pa = set_empty<set_type>();

    // let's count X0=0 and [X1=0,X2=1]:

    // first variables
    xi = set_add(xi, 0);
    pa = set_add(pa, 1);
    pa = set_add(pa, 2);

    // then states
    std::vector<char> sxi{0};
    std::vector<char> spa{0, 1};

    // callback for each xi
    std::vector<call> C(1);

    // and here we go
    bvc.apply(xi, pa, sxi, spa, C);

    std::cout << C[0].score() << std::endl;

    return 0;
} // main
