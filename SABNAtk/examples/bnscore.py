import csv
import sys

from bitarray import bitarray

from sabnatk.BVCounter import AIC64
from sabnatk.BVCounter import BDeu64
from sabnatk.BVCounter import MDL64


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("usage: bnscore.py data network")
        exit(-1)

    data = sys.argv[1]

    n = 0
    m = 0
    D = []

    # read data
    with open(data, "rt") as cf:
        rd = csv.reader(cf, delimiter = ' ')
        for row in rd:
            n = n + 1
            m = len(row)
            D = D + row

    D = list(map(int, D))

    # init graph
    G = []

    for i in range(n):
        u = bitarray(n, endian = "little")
        u.setall(0)
        G = G + [u]

    # read network
    net = sys.argv[2]

    with open(net, "rt") as nf:
        for l in nf.readlines():
            s, _, t = l.rstrip().split(" ")
            if (s != t):
                G[int(t)][int(s)] = 1;

    # init score
    s = MDL64()
    s.init(n, m, D)

    # get score
    score = 0.0

    for i in range(n):
        xi = bitarray(n, endian = "little")
        xi.setall(0)
        xi[i] = 1
        score = score + s.score(xi.tobytes(), G[i].tobytes())[0][0]

    print(score)
