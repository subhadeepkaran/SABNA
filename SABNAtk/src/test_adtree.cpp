#include <algorithm>
#include <iostream>
#include <vector>
#include <tuple>

#include "bit_util.hpp"
#include "csv.hpp"

#include "ADTree.hpp"
#include "BLANK.hpp"
#include "BLANKEngine.hpp"
#include "MDL.hpp"
#include "MDLEngine.hpp"
#include "BDeu.hpp"
#include "BDeuEngine.hpp"

#include "BVCounter.hpp"


int main(int argc, char* argv[]) {
    using data_type = signed char;
    using set_type = uint_type<1>;
    

    int n = -1;
    int m = -1; 
   

    std::vector<data_type> D;

    bool st;
    std::tie(st, n, m) = read_csv(argv[1], D);


    if (!st) {
        std::cout << "can't read the file" << std::endl;
        return -1;
    }   

    double alpha = 1.0;

    set_type pa = set_empty<set_type>();
    pa = set_add(pa, 1);

    set_type ch = set_empty<set_type>();
    ch = set_add(ch, 0);
    ch = set_add(ch, 2);

    ADTree<1, data_type> adt(0, n, m, D);
    std::cout << "min leaf: " << 0 << "      n: " << n << "   m: " << m << "   Dsize: " << D.size() << std::endl;
    adt.build_tree();

    BVCounter<1> bvc = create_BVCounter<1>(n, m, std::begin(D));	

    std::vector<MDL> v_mdl1(set_size(ch), MDL(m));    
    std::vector<MDL> v_mdl2(set_size(ch), MDL(m));    

    adt.apply(ch, pa, v_mdl1); 
    bvc.apply(ch, pa, v_mdl2); 

    for (int idx = 0; idx < set_size(ch); ++idx) {
        std::cout << "idx: " << idx << std::endl;
        std::cout << "     adt " << v_mdl1[idx].score().first << std::endl;  
        std::cout << "     bvc " << v_mdl2[idx].score().first << std::endl;  
    }

    return 0;
}
