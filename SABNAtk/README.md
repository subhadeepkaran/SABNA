# SABNAtk: Fast Counting in Machine Learning Applications

**Authors:**
Subhadeep Karan <skaran@buffalo.edu>,
Matthew Eichhorn <maeichho@buffalo.edu>,
Blake Hurlburt <blakehur@buffalo.edu>,
Grant Iraci <grantira@buffalo.edu>,
Jaroslaw Zola <jaroslaw.zola@hush.com>

## About

SABNAtk is a small C++14 library, together with Python bindings, to efficiently execute counting queries over categorical data. Such queries are common to Machine Learning applications, for example, they show up in Probabilistic Graphical Models, regression analysis, etc.. In practical applications, SABNAtk [significantly outperforms](https://gitlab.com/SCoRe-Group/SABNAtk-Benchmarks) typical approaches based on e.g., hash tables or ADtrees. Currently, SABNAtk is powering [SABNA](https://gitlab.com/SCoRe-Group/SABNA-Release), our Bayesian networks learning engine. We are working on further improving performance, so stay tuned!

## User Guide

In preparation - we will provide extended documentation soon. Please, refer to `examples/` directory to see several use examples. If you have immediate questions, please do not hesitate to contact Jaric Zola <jaroslaw.zola@hush.com>.

## References

To cite SABNAtk, refer to this repository and our 2018 UAI paper:

* S. Karan, M. Eichhorn, B. Hurlburt, G. Iraci, J. Zola, _Fast Counting in Machine Learning Applications_, In Proc. Uncertainty in Artificial Intelligence (UAI), 2018. <https://arxiv.org/abs/1804.04640>.
