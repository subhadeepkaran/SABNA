/***
 *    $Id$
 **
 *    File: SparseContab.hpp
 *    Created: Oct 2, 2017
 *
 *    Authors: Subhadeep Karan <skaran@buffalo.edu>
 *    Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *    Distributed under the MIT License.
 *    See accompanying file LICENSE.
 */


#ifndef SPARSE_CONTAB_HPP
#define SPARSE_CONTAB_HPP

class SparseContab {
public:
    SparseContab(const int val, const bool is_leaf, int state) {
        state_ = state;
        val_ = val;
        is_leaf_ = is_leaf; 
    } // SparseContab

    void remove_sub_contab(SparseContab*& cont) {
        if (cont != nullptr) {
            cont->remove_all_sub_contab();
            delete cont;
            cont = nullptr;
        }
    } // remove_sub_contab

    void remove_all_sub_contab() {
        for (auto& x : ch_) { remove_sub_contab(x); }
        ch_.clear();
    } // remove_all_sub_contab

    ~SparseContab() { remove_all_sub_contab(); } // ~SparseContab

    void initialize_all_sub_contab(int r) {
        ch_.resize(r, nullptr);
        is_leaf_ = false;
    } //

    int get_state() const { return state_; }

    int get_sub_contab_size() const { return ch_.size(); } 

    int get_value() const { return val_; } 

    bool is_leaf() const { return is_leaf_; }

    void set_ch(int idx, SparseContab*& cont) {
        if (cont != nullptr) { ch_[idx] = cont; } 
    } // set

    void set_val(int val) { val_ = val; } 

    void set_as_leaf(int count) { 
        is_leaf_ = true; 
        val_ = count;
        remove_all_sub_contab();
    }

    SparseContab* get_sub_contab(int idx) { return ch_[idx]; }

    SparseContab* subtract(SparseContab*& other) {
        if (other == nullptr) { return this; }
        
        struct stack_element {
            SparseContab* lhs;
            SparseContab* rhs;
            SparseContab* pa_lhs;
            int idx;
            int state;
        };
        
        std::vector<stack_element> stack;
        stack.push_back(stack_element{this, other, nullptr, -1, 0}); 

        while (!stack.empty()) {
            SparseContab*& lhs = stack.back().lhs;
            SparseContab*& rhs = stack.back().rhs;
            int& idx = stack.back().idx;
           
            if (lhs->is_leaf_ && rhs->is_leaf_) {
                lhs->val_ -= rhs->val_;
            
                if (lhs->val_ == 0) { 
                    delete lhs;
                    stack.back().pa_lhs->ch_[stack.back().state] = nullptr;
                }
                stack.pop_back();
                continue;
            }
            
            ++idx;
            if (idx >= lhs->ch_.size() || idx >= rhs->ch_.size()) {
                stack.pop_back();
                continue;
            }

            if (lhs->ch_[idx] == nullptr || rhs->ch_[idx] == nullptr) { continue; }    
            stack.push_back({lhs->ch_[idx], rhs->ch_[idx], lhs, -1, idx});
        }

    } // subtract

    int state_;
private:
    int val_;
    std::vector<SparseContab*> ch_;
    bool is_leaf_;
}; // class Contab

#endif 
