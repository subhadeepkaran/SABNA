/***
 *    $Id$
 **
 *    File: ADTree.hpp
 *    Created: Sept 20, 2017
 *
 *    Authors: Subhadeep Karan <skaran@buffalo.edu>
 *    Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *    Distributed under the MIT License.
 *    See accompanying file LICENSE.
 */

#ifndef AD_TREE_HPP
#define AD_TREE_HPP

#include <algorithm>
#include <cstring>
#include <iostream>
#include <limits>
#include <tuple>
#include <vector>
#include <string>

#include <boost/dynamic_bitset.hpp>
#include <boost/variant.hpp>

#include "bit_util.hpp"

#include "SparseContab.hpp"


template<int N, typename data_type>
class ADTree {
using set_type = uint_type<N>;
public:
    ADTree(int min_leaf_size, int n, int m, const std::vector<data_type> D) : min_leaf_size_(min_leaf_size), n_(n), m_(m), D_(D), E_(set_empty<set_type>()) { }

    int n() const { return n_; }

    int m() const { return m_; }

    int r(int xi) const { return r_[xi]; }

    std::pair<bool, std::string> build_tree() {
        if (min_leaf_size_ < 0) { return {"false", "minimum leaf size should be positive"}; }

        r_.resize(n_, -1);
        Dbit_idx_.resize(n_, -1);

        int r_sum = 0;
        for (int i = 0; i < n_; ++i) {
            auto min_max = std::minmax_element(D_.begin() + i*m_, D_.begin() + (i+1)*m_);
            std::transform(D_.begin() + i*m_, D_.begin() + (i+1)*m_, D_.begin() + i * m_, [min_max](data_type x) { return x - *(min_max.first); } );
            r_[i] = *min_max.second - *min_max.first + 1;
            if (r_[i] == 1) { return {false, "var " + std::to_string(i) + " has one only category"}; } 
            Dbit_idx_[i] = r_sum;
            r_sum += r_[i];
        }

        Dbit_.resize(r_sum, boost::dynamic_bitset<>(m_));
        m_transform2bitwise__();       

        tree_.push_back(ad_tree_node{NODE_TYPE::NORMAL_NODE, m_, std::vector<int>(n_, NODE_TYPE::NULL_NODE)});

        ad_tree_node& rt_ad_tree = tree_[0];
        
        if (m_ <=  min_leaf_size_) { 
            rt_ad_tree.state_ = NODE_TYPE::LEAFLIST_NODE;
            rt_ad_tree.index_.resize(m_);
            for (int i =0; i < m_; ++i) { rt_ad_tree.index_[i] = i; }
        }

        m_build_tree__();
        
        Dbit_.clear();  
        Dbit_idx_.clear();

        return {true, ""};
    } // build_tree

    bool reorder(const std::vector<int>& order) { return false; }

    template<typename score_functor>
    void apply(const set_type set_xi, const set_type pa, const std::vector<data_type>& state_xi, const std::vector<data_type>& state_pa, std::vector<score_functor>& F) {
        std::vector<SparseContab*> vec_root(set_size(set_xi), nullptr);
        m_get_sparse_contab_root__(set_xi, pa, vec_root, F, 1);

        for (int xi = 0, idx_xi = 0; xi < n_; ++xi) {
            if (!in_set(set_xi, xi)) { continue; }
            
            SparseContab* cont = vec_root[idx_xi];
            std::vector<int> vec_nijk(r_[xi], 0);


            int l = state_pa.size() + 1;
            for (int xj = 0, idx_pa = 0; xj < n_; ++xj) {
                if (in_set(pa, xj) || (xi == xj)) {
                    data_type temp;
                    if (xi == xj) { temp = state_xi[idx_xi]; } 
                    else { temp = state_pa[idx_pa++]; }
                    cont = cont->get_sub_contab(temp);
                    --l;
                    if (cont == nullptr || cont->is_leaf()) { break; }
                }
            }
           
            if (l == 0 && cont != nullptr) {
                F[idx_xi](-1);
                F[idx_xi](cont->get_value(), -1);
            } 

            ++idx_xi;
        }

        for (auto cont : vec_root) {
            cont->remove_all_sub_contab();
            delete cont;
        }

    } // apply (state_specific_queries)

    template<typename score_functor>
    void apply(const set_type& set_xi, const set_type& pa, std::vector<score_functor>& F) {
        std::vector<SparseContab*> vec_root(set_size(set_xi), nullptr);
        int q_pa = m_compute_q_pa__(pa);
        int pa_size = set_size(pa);

        m_get_sparse_contab_root__(set_xi, pa, vec_root, F, q_pa);
        
        for (int xi = 0, idx_xi = 0; xi < n_; ++xi) {
            if (!in_set(set_xi, xi)) { continue; }
            
            std::vector<contab_row> final_table;
            m_tabularize_contab__(vec_root[idx_xi], xi, set_add(pa, xi), final_table, pa_size+1);
            
            vec_root[idx_xi]->remove_all_sub_contab();
            delete vec_root[idx_xi];
            vec_root[idx_xi] = nullptr;

            if (final_table.empty()) { return; }
            auto ref_row = final_table[0].row_;

            int Nij = 0;
            int qi_obs;
            int start = 0;
            int end = 0;
            for (int i = 0; i < final_table.size(); ++i) {
                if (ref_row != final_table[i].row_) { 
                    ++qi_obs;
                    ref_row = final_table[i].row_; 
                    start = end;
                    end = i;
                    F[idx_xi](Nij);
                    for (; start < end; ++start) { F[idx_xi](final_table[start].count_, Nij); }
                    Nij = 0;
                }
                Nij += final_table[i].count_;
            }
            ++qi_obs;
            F[idx_xi](Nij);
            for (; start < final_table.size(); ++start) { F[idx_xi](final_table[start].count_, Nij); }
            
            ++idx_xi;
            //uncomment to set qi = number of observed parent set configuration states
            //F[idx_xi].finalize(qi_obs);
        } // for xi

    } // apply

private:
    struct ad_tree_node {
        int state_;
        union {
            int count_;
            int mcv_;
        }get;
        std::vector<int> index_;
        
        bool is_leaf() const { return state_ == NODE_TYPE::LEAF_NODE; }
        bool is_leaflist() const { return state_ == NODE_TYPE::LEAFLIST_NODE; }
        bool is_ch_ad_node_null(int r) const { return index_[r] == NODE_TYPE::NULL_NODE; }
    };
   
    struct contab_row {
        int count_;
        int row_sum_;
        int xi_state_;
        std::vector<int> row_;
        bool operator<(const contab_row& rhs) {
            for (int i = 0; i < row_.size(); ++i) {
                if (row_[i] == rhs.row_[i]) { continue; }
                return row_[i] < rhs.row_[i];
            }
            return xi_state_ < rhs.xi_state_;
        }
    };

    template<typename score_functor>
    void m_get_sparse_contab_root__(const set_type set_xi, const set_type pa, std::vector<SparseContab*>& vec_root, std::vector<score_functor>& F, int q_pa) {
        int pa_size = set_size(pa);

        for (int xi = 0, idx_xi = 0; xi < n_; ++xi) {
            if (!in_set(set_xi, xi)) { continue; }
            F[idx_xi].init(r_[xi], q_pa);

            vec_root[idx_xi] = new SparseContab(0, true, -1);
            set_type node_id = set_add(pa, xi);

            const ad_tree_node& rt_ad = tree_[0];
            if (rt_ad.is_leaflist()) { m_build_contab_leaflist__(node_id, tree_[0], vec_root[idx_xi]); }
            else if (!rt_ad.is_leaf()) { m_build_contab__(node_id, -1, 0, vec_root[idx_xi]);  }
            
            ++idx_xi;
        }
    } // m_get_sparse_contab_root__

    // tabularizes the sparse contab and then removes the sparse contab 
    void m_tabularize_contab__(SparseContab* cont, int xi, set_type node_id, std::vector<contab_row>& final_table, int id_size) {
            final_table.reserve(m_);

            std::vector<int> vec;
            vec.reserve(id_size);

            int idx_xi = 0;
            for (int xj = 0; xj < n_; ++xj) {
                if (!in_set(node_id, xj)) { continue; }
                if (xi == xj) { break; }
                ++idx_xi;
            }

            std::vector<std::pair<int, SparseContab*>> stack;
            stack.reserve(n_);
            stack.push_back({-1, cont});

            while (!stack.empty()) {
                int& l = stack.back().first;
                SparseContab*& cont = stack.back().second;

                if (++l >= cont->get_sub_contab_size() || cont->is_leaf()) {
                    stack.back().second->remove_all_sub_contab();
                    stack.pop_back(); 
                    vec.pop_back(); 
                    continue; 
                }
                        
                SparseContab* next_cont = cont->get_sub_contab(l);
                if (next_cont == nullptr) { continue; }
                stack.push_back({-1, next_cont}); 
                vec.push_back(next_cont->get_state());
                
                if (vec.size() == id_size) {
                    int state_xi = vec[idx_xi];
                    vec[idx_xi] = 0;
                    final_table.push_back({next_cont->get_value(), 0, state_xi, vec});
                    vec[idx_xi] = state_xi;
                }
            } // while

            if (idx_xi != id_size) { std::sort(final_table.begin(), final_table.end()); } 
    } // m_tabulairze_contab__

    void m_build_contab__(const set_type& node_id, int bias, const int idx_ad, SparseContab*& f_cont) {
        struct stack_element {
            set_type node_id;
            int min_xi;
            int bias;
            int idx_ad;
            int idx_vn;
            SparseContab* f_cont;
            SparseContab* cont;
            int r;
        };

        std::vector<stack_element> stack;
        stack.push_back({node_id, -1, bias, idx_ad, -1, f_cont, nullptr, -1});

        while(!stack.empty()) {
            const set_type& node_id = stack.back().node_id;
            int& min_xi = stack.back().min_xi;
            const int bias = stack.back().bias;
            const int& idx_ad = stack.back().idx_ad;
            int& idx_vn = stack.back().idx_vn;
            SparseContab*& f_cont = stack.back().f_cont;
            SparseContab*& cont = stack.back().cont; 
            int& r = stack.back().r;
            
            if (node_id == E_) {
                f_cont->set_as_leaf(tree_[idx_ad].get.count_);
                stack.pop_back();
                continue;
            }

            if (r == -1 && min_xi == -1) {
                min_xi = 0;
                for (; min_xi < n_ && !in_set(node_id, min_xi); ++min_xi);
                idx_vn = tree_[idx_ad].index_[min_xi-bias-1];
                cont = new SparseContab(0, true, tree_[idx_vn].get.mcv_);
                stack.push_back({set_remove(node_id, min_xi), -1, bias, idx_ad, -1, cont, nullptr, -1});
            }
            else if (r >= r_[min_xi]) { stack.pop_back(); continue; }
            else if (r >= -1 && min_xi != -1) {
                const ad_tree_node& vn = tree_[idx_vn];
                if (cont != nullptr) { 
                    const int state = cont->get_state();
                    if (state == vn.get.mcv_) { f_cont->initialize_all_sub_contab(r_[min_xi]); }
                    f_cont->set_ch(state, cont);
                    if (state != vn.get.mcv_) { f_cont->get_sub_contab(vn.get.mcv_)->subtract(cont); }
                } 
                else if (cont == nullptr && r == vn.get.mcv_) { 
                    stack.pop_back(); 
                    continue; 
                }
                
                set_type ch_node_id = set_remove(node_id, min_xi);
                
                for (++r; r < r_[min_xi]; ++r) {
                    if (r == vn.get.mcv_ || vn.is_ch_ad_node_null(r)) { continue; }
                    
                    const ad_tree_node& ch_ad = tree_[vn.index_[r]];
                    if (ch_ad.get.count_ == 0) { continue; }
                    
                    cont = new SparseContab(0, true, r);
                    
                    if (!ch_ad.is_leaflist() || ch_node_id == E_) {
                        stack.push_back({ch_node_id, -1, min_xi, vn.index_[r], -1, cont, nullptr, -1});
                        break;
                    }
                    else if (ch_ad.get.count_ != 0 && ch_ad.is_leaflist()) { 
                        m_build_contab_leaflist__(ch_node_id, ch_ad, cont); 
                        f_cont->set_ch(r, cont);
                        f_cont->get_sub_contab(vn.get.mcv_)->subtract(cont);
                    }
                } // for r
            } // elif
        } // while

    } // m_build_contab__

    void m_build_contab_leaflist__(const set_type& node_id, const ad_tree_node& rt_ad, SparseContab*& f_cont) {
        int min_xi = 0;
        for (; min_xi < n_ && !in_set(node_id, min_xi); ++min_xi); 

        int max_xi = n_ - 1;
        for (; max_xi > min_xi && !in_set(node_id, max_xi); --max_xi);

        f_cont->initialize_all_sub_contab(r_[min_xi]);
        std::vector<SparseContab*> stack(rt_ad.index_.size(), f_cont); 

        for (int xi = min_xi; xi < n_; ++xi) {
            if (!in_set(node_id, xi)) { continue; }
            int next_xi = xi + 1;
            for (; next_xi < n_ && !in_set(node_id, next_xi); ++next_xi);
            int idx_stack = 0;
            for (const int& idx : rt_ad.index_) {
                int r = static_cast<int>(D_[xi * m_ + idx]);
                SparseContab*& temp = stack[idx_stack];
                if (temp->get_sub_contab(r) != nullptr) {
                    int val = temp->get_sub_contab(r)->get_value();
                    temp->get_sub_contab(r)->set_val(++val);
                } else  {
                    SparseContab* cont = new SparseContab(1, true, r);
                    if (next_xi <= max_xi) { cont->initialize_all_sub_contab(r_[next_xi]); }
                    temp->set_ch(r,  cont);
                }
                stack[idx_stack] = temp->get_sub_contab(r);
                ++idx_stack;
            }
        } // for xi
 
    } // m_build_contab_leaflist_
    
    void m_build_tree__() { 
        if (tree_[0].is_leaflist()) { return; }

        tree_.reserve(10000);
           
        struct stack_element {
            int ad_idx;
            int xi;
            int vy_idx;
            int xj;
            int r;
            boost::dynamic_bitset<> pD;
            int mcv;
            int popcount_mcv;
            boost::dynamic_bitset<> D_mcv;
        };
        
        boost::dynamic_bitset<> pD(m_);
        pD.set();

        std::vector<stack_element> stack;
        stack.push_back({0, -1, NODE_TYPE::NULL_NODE, 0, -1, pD, 0, -1});

        while(!stack.empty()) {
            const int ad_idx = stack.back().ad_idx;
            const int xi  = stack.back().xi; 
            int& vy_idx = stack.back().vy_idx;
            int& xj  = stack.back().xj; 
            int& r   = stack.back().r;

            if (xj >= n_) {
                stack.pop_back();
                continue;
            }
 
            if (r >= r_[xj] - 1) {
                ++xj;
                vy_idx = NODE_TYPE::NULL_NODE;
                r = -1;
                continue;
            }

            boost::dynamic_bitset<> pD = stack.back().pD;
            int& mcv = stack.back().mcv;
            int& popcount_mcv = stack.back().popcount_mcv;
            boost::dynamic_bitset<>& D_mcv = stack.back().D_mcv;
            if (vy_idx == NODE_TYPE::NULL_NODE) {
                vy_idx = tree_.size();
                mcv = 0;
                r = 0;
                D_mcv = Dbit_[Dbit_idx_[xj]] & pD;
                popcount_mcv = D_mcv.count();
                tree_.push_back({0, mcv, std::vector<int>(r_[xj], NODE_TYPE::NULL_NODE)});
                tree_[ad_idx].index_[xj - xi - 1] = vy_idx;
            }

            int cand_mcv = ++r;          
            boost::dynamic_bitset<> D_cand_mcv = pD & Dbit_[Dbit_idx_[xj] + cand_mcv];
            int popcount_cand_mcv = D_cand_mcv.count();  
         
            if (popcount_cand_mcv == 0) continue; 
 
            if (popcount_mcv < popcount_cand_mcv) {
                D_cand_mcv.swap(D_mcv);
                int tmp = mcv;
                mcv = cand_mcv;
                cand_mcv = tmp;
                tree_[vy_idx].get.mcv_ = mcv;
                tmp = popcount_cand_mcv;
                popcount_cand_mcv = popcount_mcv;
                popcount_mcv = tmp;
            }

            tree_[vy_idx].index_[cand_mcv] = tree_.size(); 
            tree_.push_back({NODE_TYPE::NORMAL_NODE, popcount_cand_mcv}); 

            if (xj == n_ - 1 || popcount_cand_mcv <= min_leaf_size_) {
                if (xj != n_ - 1) {
                    tree_.back().state_ = NODE_TYPE::LEAFLIST_NODE;
                    tree_.back().index_.reserve(popcount_cand_mcv);
                    for (int i = 0; i < m_ && popcount_cand_mcv > 0; ++i) { if (D_cand_mcv[i]) { tree_.back().index_.push_back(i); } }   
                }
                else { tree_.back().state_ = NODE_TYPE::LEAF_NODE; }
                continue; 
            }
            tree_.back().index_.resize(n_ - xj - 1, NODE_TYPE::NULL_NODE);
            stack.push_back({static_cast<int>(tree_.size()) - 1, xj, NODE_TYPE::NULL_NODE, xj+1, -1, D_cand_mcv, 0, popcount_cand_mcv});
        }   
    
        tree_.resize(tree_.size());
    } // build_tree__

    void m_transform2bitwise__() {
        for (int i = 0, idx = 0; i < n_; ++i) {
            for (int j = 0; j < m_; ++j) { Dbit_[idx + D_[i * m_ + j]][j] = 1; }
            idx += r_[i];
        }
    } // m_transform2bitwise__

    int m_compute_q_pa__(const set_type& pa) const {
        int q = 1;

        for (int xi = 0; xi < n_; ++xi) {
            if (!in_set(pa, xi)) { continue; }
            q *= r_[xi];
        }

        return q;
    } // m_compute_q_pa__

    enum NODE_TYPE {NULL_NODE = -1, NORMAL_NODE = 0, LEAF_NODE = 1, LEAFLIST_NODE = 2};

    const int min_leaf_size_;

    const int n_;
    const int m_;

    const set_type E_;

    std::vector<data_type> D_;
    std::vector<int> r_;
    std::vector<boost::dynamic_bitset<>> Dbit_;
    std::vector<int> Dbit_idx_;
      
    std::vector<ad_tree_node> tree_;
}; // class ADTree


#endif
